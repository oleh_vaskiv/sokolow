<?php


namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LocationsAdmin extends Admin
{

    // setup the default sort column and order
    protected $datagridValues = array(
        //'_sort_order' => 'DESC',
        //'_sort_by' => 'id'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Gallery');
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('LsCmsBundle:Gallery', 'g')
            ->where('g.attachable = 1')
            ->getQuery();*/

        $helps = array();
        $formMapper
            ->with('Treść')
            ->add('name', null, array('label' => 'Nazwa', 'required' => true))
            ->add('address', null, array('label' => 'Adres', 'required' => true));
            /*
            ->add('gallery', 'sonata_type_model', array('query' => $query, 'label' => 'Galeria', 'required' => false))
            ->with('Zdjęcie');
            if (null !== $this->getRoot()->getSubject()->getPhoto()) {
                $formMapper
                    ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
            } else {
                $formMapper
                    ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true));
            }

        $formMapper->setHelps($helps);
            */
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Nazwa'))
            ->add('address', null, array('label' => 'Adres'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', null, array('label' => 'Nazwa'))
            ->add('gallery', null, array('label' => 'Galeria', 'template' => 'LsCmsBundle:Admin\Porada:list_gallery.html.twig'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Locations:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        //if (null !== $entity->getFile()) {
        //    $sFileName = uniqid('cars-image-') . '.' . $entity->getFile()->guessExtension();
        //    $entity->setPhoto($sFileName);
        //    $entity->upload();
        //}
    }

    public function preUpdate($entity) {
        //if (null !== $entity->getFile()) {
        //    $sFileName = uniqid('cars-image-') . '.' . $entity->getFile()->guessExtension();
        //    $entity->setPhoto($sFileName);
        //    $entity->upload();
        //}
    }
}

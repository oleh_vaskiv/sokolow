<?php
/**
 * Created by PhpStorm.
 * User: Lukasz
 * Date: 08.10.14
 * Time: 17:21
 */

$tableName = 'locations';
$className = 'Locations';
$items = array(
    'name','address'
);

echo '
<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * Cars
 * @ORM\Table(name="'.$tableName.'")
 * @ORM\Entity
 */
class '.$className.' extends BaseEntity {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

';


foreach($items as $item){

echo
'
/**
* @ORM\Column(type="string", length=255)
* @var string
*/
private $'.$item.';
';

}




foreach($items as $item){

echo '

/**
* Set '.$item.'
*
* @param string $'.$item.'
* @return '.$className.'
*/
public function set'.ucfirst($item).'($'.$item.') {
    $this->'.$item.' = $'.$item.';

    return $this;
}

/**
* Get '.$item.'
*
* @return string
*/
 public function get'.ucfirst($item).'() {
     return $this->'.$item.';
 }


';

}

echo '
}
';
-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 23 Gru 2016, 14:15
-- Wersja serwera: 5.5.53-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Baza danych: `lemonade_15`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_group`
--

CREATE TABLE `fos_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admin', 'admin', 'test@test.pl', 'test@test.pl', 1, 'aqn5nzng0dck00kk8gogwkkc8c4wooc', '5I3l3ug/NzJzzLui1jzlIooHH1Cy4jqhDaukjHC/VuOoS8Vfk8kY8RHmAgEr4ra+Wpb/sOjIJO+28U/o+7envA==', '2016-11-28 09:10:06', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-04-01 15:18:49', '2016-11-28 09:10:06', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(2, 'rafalglazar', 'rafalglazar', 'rafalglazar@gmail.com', 'rafalglazar@gmail.com', 1, 't0uk2tmpg40cog80kkg04088sc48wo4', 'viAvhoyV8qazSD+crS7R5zspKj3+Rv4aeqCH9gitAU6RKnkFIdgH19RjtXPJtj23wdrxYVczR4rKGt9uI4ILLQ==', '2014-06-27 17:50:17', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-04-01 15:19:14', '2014-06-27 17:50:17', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, 'marcin', 'marcin', 'biuro@iammusic.pl', 'biuro@iammusic.pl', 1, 's9h29nkgj2ss04g0scgc40488w40s00', 'LNwX93etNOmeKxqqGOoTpDQ0PbcXUhCeiHcfByhlMMOUvkupmddlAqKcmiwCgZmL8zwGKSPuH6bAbFgulsevyw==', NULL, 0, 0, NULL, NULL, NULL, 'a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-06-16 19:27:49', '2014-06-16 19:28:01', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(4, 'superuser', 'superuser', 'luke1990@niepodam.pl', 'luke1990@niepodam.pl', 1, 'aqn5nzng0dck00kk8gogwkkc8c4wooc', 'y1k4ZkPg+VzgX6hqiyYz9nJjsLW3FTwyS6emayNxpEL/Xf+R8dtSJ15CctpQcO08Wv6EFiFwcgHk1TDbcHJoVA==', '2014-10-26 01:08:43', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-08-13 00:20:50', '2014-10-26 01:08:43', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'piotrek', 'piotrek', 'piotr@piotrjanczarek.pl', 'piotr@piotrjanczarek.pl', 0, '5v9gjge0bf4scw4oo4ooc8k4s00w88g', 'y91LRJ1sTWT3cIZSGXIKoCfqm/gM2g3RCozdhxR2qZV/xFpwUDJad1rWIaE1I6Lubdog9r9ZMq/ZzbyTmt0FGw==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2014-08-26 01:01:07', '2014-08-26 01:01:07', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(6, 'Sylwia', 'sylwia', 'sylwia13s@gmail.com', 'sylwia13s@gmail.com', 1, 'qahocfow7ms0oow8sgwgwsc0k0okwgg', 'b4lfNKmfeUjT2bqmkq4Zo2NlwnhjYEeY7mDjaIaGnqyRa3crj+oYpH92tcqJLJ9lFDxapPlzU1ZnF1vZzACOMA==', '2014-09-04 12:20:15', 0, 0, NULL, NULL, NULL, 'a:3:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";i:2;s:22:"ROLE_ALLOWED_TO_SWITCH";}', 0, NULL, '2014-09-03 13:18:56', '2014-09-04 12:20:15', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `on_list` tinyint(1) NOT NULL DEFAULT '0',
  `attachable` tinyint(1) NOT NULL DEFAULT '0',
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `content_short`, `content`) VALUES
(3, 'Podkarpackie Smaki 2014', 'podkarpackie-smaki-2014', 1, 1, 0, 'Podkarpackie Smaki 2014', 'Podkarpackie, Smaki, 2014', '', '2014-08-15 19:17:28', '2014-10-23 13:43:48', NULL, NULL),
(24, 'Koncert Kolęd', 'koncert-koled', 1, 1, 0, NULL, NULL, NULL, '2014-10-23 13:44:15', NULL, NULL, NULL),
(25, 'Literatura i dzieci', 'literatura-i-dzieci', 1, 1, 0, NULL, NULL, NULL, '2014-10-23 13:46:10', NULL, NULL, NULL),
(26, '11 listopada 2013', '11-listopada-2013', 1, 1, 0, NULL, NULL, NULL, '2014-10-23 13:54:08', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(88, 3, 'gallery-image-5448e97fa2b8c.jpg', 1),
(89, 3, 'gallery-image-5448e98014f42.jpg', 2),
(90, 3, 'gallery-image-5448e980456fc.jpg', 3),
(91, 3, 'gallery-image-5448e98077638.jpg', 4),
(92, 3, 'gallery-image-5448e980aa001.jpg', 5),
(93, 3, 'gallery-image-5448e980db299.jpg', 6),
(94, 3, 'gallery-image-5448e9811851e.jpg', 7),
(95, 3, 'gallery-image-5448e981485ee.jpg', 8),
(96, 3, 'gallery-image-5448e981798e0.jpg', 9),
(97, 24, 'gallery-image-5448ea43bc7ca.jpg', 1),
(98, 24, 'gallery-image-5448ea4409c80.jpg', 2),
(99, 24, 'gallery-image-5448ea4434981.jpg', 3),
(100, 24, 'gallery-image-5448ea445bd3b.jpg', 4),
(101, 24, 'gallery-image-5448ea44853ae.jpg', 5),
(102, 24, 'gallery-image-5448ea44af0a5.jpg', 6),
(103, 24, 'gallery-image-5448ea44d65f9.jpg', 7),
(104, 24, 'gallery-image-5448ea4509fac.jpg', 8),
(105, 24, 'gallery-image-5448ea4529d39.jpg', 9),
(106, 25, 'gallery-image-5448eab858f66.jpg', 1),
(107, 25, 'gallery-image-5448eab8803bf.jpg', 2),
(108, 25, 'gallery-image-5448eab8a83d6.jpg', 3),
(109, 25, 'gallery-image-5448eab8cfca8.jpg', 4),
(110, 25, 'gallery-image-5448eab904f90.jpg', 5),
(111, 25, 'gallery-image-5448eab92c286.jpg', 6),
(112, 25, 'gallery-image-5448eab95348a.jpg', 7),
(113, 25, 'gallery-image-5448eab97a0df.jpg', 8),
(114, 25, 'gallery-image-5448eab99db55.jpg', 9),
(115, 26, 'gallery-image-5448eca959032.jpg', 1),
(116, 26, 'gallery-image-5448eca984f2a.jpg', 2),
(117, 26, 'gallery-image-5448eca9aef5a.jpg', 3),
(118, 26, 'gallery-image-5448eca9cebc7.jpg', 4),
(119, 26, 'gallery-image-5448eca9f2980.jpg', 5),
(120, 26, 'gallery-image-5448ecaa297ae.jpg', 6),
(121, 26, 'gallery-image-5448ecaa56213.jpg', 7),
(122, 26, 'gallery-image-5448ecaa7acfb.jpg', 8),
(123, 26, 'gallery-image-5448ecaaa3b4e.jpg', 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `karnet_zamowienie`
--

CREATE TABLE `karnet_zamowienie` (
  `id` int(11) NOT NULL,
  `karnet_id` int(11) DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `document` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ankieta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeParameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `routeParameters`, `url`, `onclick`, `arrangement`) VALUES
(2, NULL, 'menu_top', 'route', 'Urząd', 'lscms_pages_show', '{"slug":"o-mnie"}', NULL, NULL, 5),
(5, NULL, 'menu_bottom', 'route', 'Strona główna', 'lscms_homepage', NULL, NULL, NULL, 1),
(6, NULL, 'menu_bottom', 'route', 'Aktualności', 'lscms_news', NULL, NULL, NULL, 2),
(7, NULL, 'menu_bottom', 'route', 'O mnie', 'lscms_pages_show', '{"slug":"o-mnie"}', NULL, NULL, 3),
(11, NULL, 'menu_bottom', 'route', 'Moja działalność', 'lscms_dietetyk', NULL, NULL, NULL, 4),
(12, NULL, 'menu_top', 'route', 'Kontakt', 'lscms_kontakt', NULL, NULL, NULL, 9),
(13, NULL, 'menu_bottom', 'route', 'Kontakt', 'lscms_kontakt', NULL, NULL, NULL, 5),
(16, NULL, 'menu_top', 'url', 'Gmina i Miasto', NULL, NULL, '#', NULL, 2),
(17, NULL, 'menu_top', 'route', 'Strona Główna', 'lscms_homepage', NULL, NULL, NULL, 1),
(18, NULL, 'menu_top', 'route', 'Kultura, Turystyka', 'lscms_pages_show', '{"slug":"o-mnie"}', NULL, NULL, 6),
(19, NULL, 'menu_top', 'route', 'Mieszkańcy', 'lscms_pages_show', '{"slug":"o-mnie"}', NULL, NULL, 7),
(20, NULL, 'menu_top', 'route', 'Galeria', 'lscms_gallery', NULL, '#', NULL, 8),
(22, 16, 'menu_top', 'route', 'Wirtualny urząd', 'lscms_services', '{"slug":"o-mnie-galeria"}', NULL, NULL, 3),
(23, 16, 'menu_top', 'route', 'Aktualności', 'lscms_news', NULL, NULL, NULL, 4),
(24, NULL, 'left_menu', 'url', 'Lokalna grupa działania', NULL, NULL, 'http://www.eurogalicja.com.pl/', NULL, 1),
(25, NULL, 'left_menu', 'url', 'Towarzystwo Miłośników Ziemi Sokołowskiej', NULL, NULL, 'http://www.ziemiasokolowska.pl/', NULL, 2),
(26, NULL, 'left_menu', 'url', 'Stowarzyszenia', NULL, NULL, 'http://bazy.ngo.pl/search/wyniki.asp?wyniki=1&kryt_nazwa=&kryt_miasto=soko%C5%82%C3%B3w&kryt_woj=9&kryt_pola=&kryt_typ_instyt_multi=17&baza=1&szukanie=zaawans1', NULL, 3),
(27, NULL, 'left_menu', 'url', 'OSP', NULL, NULL, 'http://bazy.ngo.pl/search/wyniki.asp?wyniki=1&kryt_nazwa=Ochotnicza&kryt_miasto=&kryt_woj=9&kryt_pola=&kryt_typ_instyt_multi=17&baza=1&szukanie=zaawans1', NULL, 4),
(28, NULL, 'left_menu', 'url', 'Organizacje pożytku publicznego 1% podatku', NULL, NULL, 'http://bazy.ngo.pl/search/wyniki.asp?wyniki=1&kryt_nazwa=&kryt_miasto=soko%C5%82%C3%B3w&kryt_woj=9&kryt_krs=&kryt_pola=&szukanie=opp', NULL, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `title`, `slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(14, 3, 'Vestibulum at magna nec orci blandit scelerisque.', 'vestibulum-at-magna-nec-orci-blandit-scelerisque', 0, 'Vestibulum at magna nec orci blandit scelerisque. Sed egestas pellentesque vehicula. Phasellus sed lorem diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras sit amet lorem arcu. Etiam felis ex, pretium eu', '<div>\r\n	Vestibulum at magna nec orci blandit scelerisque. Sed egestas pellentesque vehicula. Phasellus sed lorem diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras sit amet lorem arcu. Etiam felis ex, pretium eu laoreet sit amet, imperdiet a magna. Nam at nunc a neque elementum tempor. Phasellus odio ex, suscipit in efficitur vel, tristique non elit. Nunc sed accumsan purus, id dictum ligula. Etiam a lacinia lectus, consequat pellentesque turpis. Donec dolor tellus, placerat nec arcu eu, vulputate finibus lacus. Suspendisse non tincidunt neque, eu molestie quam.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Integer odio dolor, imperdiet vel sodales vel, lacinia id nunc. Duis id fermentum turpis, non maximus eros. Praesent lobortis, eros ac vehicula vestibulum, nunc nibh pulvinar quam, vel consectetur urna ligula ut turpis. Curabitur iaculis ipsum ullamcorper faucibus eleifend. Cras eget nulla erat. Nunc interdum bibendum sollicitudin. Nullam cursus arcu a erat cursus viverra. Nullam ac magna cursus, feugiat enim at, porttitor quam. Vivamus nunc est, gravida eget finibus vitae, suscipit nec elit.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Mauris pharetra dolor justo, sit amet fringilla sapien lobortis eu. In blandit nisl sed convallis blandit. Nullam sed cursus magna. Aenean consequat lectus eu ex commodo sollicitudin. Nulla tincidunt nec eros eget accumsan. Donec malesuada elementum eros ut blandit. In vitae bibendum risus.</div>\r\n<div>\r\n	&nbsp;</div>', 'news-image-544c2f8a58177.jpeg', 0, 'Vestibulum at magna nec orci blandit scelerisque.', 'Vestibulum, magna, orci, blandit, scelerisque., egestas, pellentesque, vehicula., Phasellus, lorem', 'Vestibulum at magna nec orci blandit scelerisque. Sed egestas pellentesque vehicula. Phasellus sed lorem diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras sit amet lorem arcu.', '2014-08-21 21:45:18', '2014-10-26 01:17:31', '2014-08-21 00:00:00'),
(15, NULL, 'aktualność 2', 'aktualnosc-2', 0, 'aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2', '<p>\r\n	aktualność 2 ... aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ... aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;aktualność 2 ...&nbsp;</p>', 'news-image-544c2fca01500.jpeg', 0, 'aktualność 2', 'aktualność, ... aktualność', 'aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ... aktualność 2 ...', '2014-10-20 20:05:14', '2014-10-26 01:18:34', '2014-10-20 00:00:00'),
(16, NULL, 'aktualnosc 3', 'aktualnosc-3', 0, 'Lorem ipsum dolore, Lorem ipsum dolorevLorem ipsum doloreLorem ipsum dolore', '<p>\r\n	Lorem ipsum dolore,&nbsp;Lorem ipsum dolorevLorem ipsum doloreLorem ipsum dolore</p>', 'news-image-544c2fb6db250.jpeg', 0, 'aktualnosc 3', 'aktualnosc, Lorem, ipsum, dolore, Lorem, dolorevLorem, doloreLorem, dolore', 'Lorem ipsum dolore, Lorem ipsum dolorevLorem ipsum doloreLorem ipsum dolore', '2014-10-20 20:05:53', '2014-10-26 01:18:15', '2014-10-20 00:00:00'),
(17, NULL, 'Lorem ipsum dolore', 'lorem-ipsum-dolore', 0, 'Lorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum dolore', '<p>\r\n	Lorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum dolore</p>', 'news-image-544c2fa20f6f1.jpeg', 0, 'Lorem ipsum dolore', 'Lorem, ipsum, dolore, doloreLorem', 'Lorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum doloreLorem ipsum dolore', '2014-10-20 20:06:33', '2014-10-26 01:17:54', '2014-10-20 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `photo`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 3, 'Kim jestem...', 'o-mnie', 'news-image-543d8465b4752.jpeg', 0, NULL, '<p>\r\n	Mam 39 lat, urodziłem się w Kraśniku. Tu też mieszkam z żoną Anną, c&oacute;reczką Olą oraz najbliższą rodziną.</p>\r\n<p>\r\n	Od listopada 2010 r. jestem Radnym Rady Powiatu Kraśnickiego. Od tego czasu sprawuję funkcję wiceprzewodniczącego Komisji Edukacji i Komisji&nbsp; Rewizyjnej przy Radzie Powiatu Kraśnickiego.</p>\r\n<p>\r\n	Od listopada 2013 r. przewodniczę ponadto Komisji Rozwoju Gospodarczego.</p>\r\n<p>\r\n	W maju 2014 r. zostałem Członkiem Powiatowej Rady Gospodarczej przy Staroście Kraśnickim.&nbsp;W 2007 r. jako jeden z pierwszych w wojew&oacute;dztwie lubelskim uzyskałem stopień&nbsp; Akredytowanego Konsultanta Funduszy Europejskich, zaś od 2008 r. zostałem Ekspertem Oceny Strategicznej projekt&oacute;w składanych do dofinansowania do RPO WL 2007-2013.</p>\r\n<h2 class="title">\r\n	<br />\r\n	Doświadczenie zdobyte na przestrzeni lat:</h2>\r\n<p class="wstep">\r\n	Od 14 lat pracuję w administracji samorządowej. Swoją karierę zawodową rozpocząłem w Powiatowym Urzędzie Pracy w Kraśniku, potem pracowałem&nbsp;w Lubelskim Urzędzie Wojew&oacute;dzkim w Lublinie, a następnie w Urzędzie Gminy Kraśnik.</p>\r\n<p class="wyroznienie-2">\r\n	Od 2008 r. pracuję w Urzędzie Marszałkowskim Wojew&oacute;dztwa Lubelskiego&nbsp;w Lublinie. W latach 2011 - 2013&nbsp; pełniłem funkcję&nbsp; Dyrektora Departamentu Gospodarki i Innowacji, a w latach 2008-2011 funkcję Zastępcy Dyrektora&nbsp;Departamentu Gospodarki i Innowacji.</p>\r\n<p class="wstep">\r\n	W Departamencie odpowiadałem, m.in. za wspieranie rozwoju przedsiębiorczości, stref ekonomicznych i teren&oacute;w inwestycyjnych, infrastruktury dostępu do Internetu, promocję gospodarczą wojew&oacute;dztwa, opracowywanie dokument&oacute;w strategicznych wojew&oacute;dztwa oraz opracowywanie i realizację prorozwojowych projekt&oacute;w własnych Samorządu Wojew&oacute;dztwa Lubelskiego dofinansowanych ze środk&oacute;w unijnych i szwajcarskich.</p>\r\n<p>\r\n	<span class="wyroznienie-1">W styczniu 2014 r. zostałem Pełnomocnikiem Marszałka Wojew&oacute;dztwa&nbsp;Lubelskiego ds. Cyfryzacji.</span></p>\r\n<p>\r\n	Ponadto jestem autorem, wsp&oacute;łautorem i koordynatorem ponad&nbsp; 60 projekt&oacute;w infrastrukturalnych i społecznych dofinansowanych ze środk&oacute;w przedakcesyjnych, unijnych, norweskich, szwajcarskich i rządowych oraz autorem lub wsp&oacute;łautorem ponad 50 strategii rozwoju, plan&oacute;w odnowy miejscowości i program&oacute;w gospodarczych wdrażanych na terenie powiatu kraśnickiego i wojew&oacute;dztwa lubelskiego w latach 2003-2014.&nbsp;</p>', 0, 'O mnie', 'O mnie', NULL, '2014-04-02 12:37:09', '2014-10-14 22:15:34');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `partners`
--

INSERT INTO `partners` (`id`, `name`, `photo`, `url`, `arrangement`) VALUES
(2, 'LCOI System', 'partners-image-54400740302d1.png', 'http://www.lcoi.pl/', 0),
(3, 'przyrodniczy szlak', 'partners-image-54400b6fe101c.png', 'http://www.puszczasandomierska.lasy.gov.pl/', 0),
(4, '365 podkarpacia', 'partners-image-54400b9faf61a.png', 'http://www.365podkarpacia.pl/', 0),
(5, 'Mgok SIR', 'partners-image-54400bba0f1ad.png', 'http://mgoksir-sokolow.pl/', 0),
(6, 'COIE', 'partners-image-54400c31c08ed.png', 'http://podkarpackie.coie.gov.pl/pl/', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_main` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `dietetyk` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  `photo_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_main` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person`
--

INSERT INTO `person` (`id`, `firstname`, `lastname`, `content_main`, `content`, `dietetyk`, `arrangement`, `photo_list`, `photo_main`) VALUES
(1, 'Katarzyna', 'Makarska', '<p>\r\n	<strong>Lek.med.</strong>&nbsp;- Lekarz konsultant. Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku. Na codzień pracuję w Radzyńskim szpitalu. Pacjenci wypowiadają się o niej ciepło i serdecznie.</p>', '<p>\r\n	<strong><span class="green">Lek.med. Katarzyna Makarska - Lekarz konsultant. Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</span></strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="Standard">\r\n	<span lang="DE">Na codzień pracuję w Radzyńskim szpitalu. Jak m&oacute;wią jej pacjenci &quot; Pani Kasia jest ciepłą i serdeczną osobą, kt&oacute;ra wkłada serce w to co robi &quot;<o:p></o:p></span></p>\r\n<p class="Standard">\r\n	<span lang="DE">&nbsp;</span></p>\r\n<p class="Standard">\r\n	<span lang="DE">W centrum zajmuje się konsultacją Naszych Pacjęt&oacute;w tak aby diety oraz ćwiczenia były zawsze dobrane w spos&oacute;b bezpieczny.<o:p></o:p></span></p>', 1, 2, 'person-5374b62bc00a7.jpeg', 'person-539e0c236f319.jpeg'),
(2, 'Marcin', 'Makarski', NULL, '<p>\r\n	<span class="green"><strong>Odpowiedzialny za motywację. Przez rok odmienił swoje życie i schudł 117 kg.</strong></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p class="Standard" style="line-height:13.5pt">\r\n	<span lang="DE">Zna proces odchudzania można powiedzieć od kuchni. W centrum wspiera osoby kt&oacute;re najczęściej są na początku swojej drogi w odchudzaniu.<o:p></o:p></span></p>', 0, 4, 'person-5374b0d3b4e56.jpeg', NULL),
(4, 'Agnieszka', 'Włoszek', NULL, '<p class="Standard">\r\n	<strong><span class="green"><span lang="DE">Instruktor Kulturystyki, absolwentka Akademii Wychowania Fizycznego w Białej Podlaskiej. </span></span></strong></p>\r\n<p class="Standard">\r\n	&nbsp;</p>\r\n<p class="Standard">\r\n	<span lang="DE">Nieustanie poszerza swoją wiedze na szkoleniach. W centrum zajmuje się układaniem trening&oacute;w w taki spos&oacute;b aby były one możliwe do zralizowania nawet dla takich os&oacute;b kt&oacute;re nigdy nie uprawiały żadnych sport&oacute;w. &nbsp;&nbsp;<o:p></o:p></span></p>', 0, 3, 'person-5374b27f814f4.jpeg', 'person-5374b27f916c4.jpeg'),
(5, 'Katarzyna', 'Witczak', '<p>\r\n	<strong>Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</strong> Studia magisterskie ukończyła z wyr&oacute;żnieniem. Obecnie jest pracownikiem naukowo-dydaktycznym Uniwersytetu w Białymstoku.</p>', '<p>\r\n	<strong><span class="green">Absolwentka dietetyki na Uniwersytecie Medycznym w Białymstoku.</span></strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Studia magisterskie ukończyła z wyr&oacute;żnieniem. Obecnie jest pracownikiem naukowo-dydaktycznym Uniwersytetu w Białymstoku. Posiada zaawansowaną wiedzę i umiejętności w zakresie żywienia człowieka zdrowego i chorego oraz profilaktyki chor&oacute;b dietozależnych.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Jak sama m&oacute;wi najważniejszym celem jej pracy z pacjentem jest ukształtowanie prawidłowych nawyk&oacute;w zywieniowych i modyfikacja jego stylu życia. Swoją wiedzę stale uaktualnia uczestnicząc w konferencjach naukowych i szkoleniach. W pracy kieruje się zasadą - <strong>Odchudzanie powinno być ratunkiem a nie udręką!</strong></p>', 1, 1, 'person-5374b6d0f3e95.jpeg', 'person-5374b6d10f521.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person_wizyta`
--

CREATE TABLE `person_wizyta` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `termin` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person_wizyta`
--

INSERT INTO `person_wizyta` (`id`, `person_id`, `firstname`, `lastname`, `phone`, `email`, `termin`, `content`) VALUES
(1, 5, 'Natalia', 'Lipska', '606 816 852', 'natalia.lipska@poczta.fm', '2014-07-17 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `porada`
--

CREATE TABLE `porada` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `page_type` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` longtext COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `porada`
--

INSERT INTO `porada` (`id`, `gallery_id`, `category_id`, `project_id`, `page_type`, `title`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(20, NULL, 6, NULL, 0, 'Konferencja - Prezentacja oferty inwestycyjnej Miasta Kraśnik', 'konferencja-prezentacja-oferty-inwestycyjnej-miasta-krasnik', '<p>\r\n	<span style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 12.8000001907349px; line-height: 18px; text-align: center; background-color: rgb(255, 255, 255);">3 kwietnia 2014 r. uczestniczyłem w imieniu Pana Andrzeja Maja - Starosty Kraśnickiego w konferencji poświęconej prezentacji oferty inwestycyjnej Miasta Kraśnik, kt&oacute;ra odbyła się w Centrum Kultury i Promocji w Kraśniku.</span></p>', '<p>\r\n	W dniu 3 kwietnia 2014 r. &nbsp;uczestniczyłem w imieniu Pana Andrzeja Maja - Starosty Kraśnickiego w konferencji poświęconej prezentacji oferty inwestycyjnej Miasta Kraśnik, kt&oacute;ra odbyła się w Centrum Kultury i Promocji w Kraśniku.&nbsp;</p>\r\n<p>\r\n	Tematem przewodnim spotkania była prezentacja oferty inwestycyjnej Kraśnika. Przewodniczący komisji rozwoju gospodarczego w Radzie Powiatu Kraśnickiego - Piotr Janczarek - zwr&oacute;cił szczeg&oacute;lną uwagę na potrzebę &nbsp;wspierania lokalnych przedsiębiorc&oacute;w. &nbsp;W nadrzędnych tematach gospodarczych należy łączyć siły, gdyż wiele działań będzie realizowanych na etapie Starostwa Powiatowego.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>', 'porada-image-540736a8e5e11.jpeg', 0, 'Konferencja - Prezentacja oferty inwestycyjnej Miasta Kraśnik', 'Konferencja, Prezentacja, oferty, inwestycyjnej, Miasta, Kraśnik, Tematem, przewodnim, spotkania, była', 'Tematem przewodnim spotkania była prezentacja oferty inwestycyjnej Kraśnika.', '2014-09-03 17:41:28', '2014-09-03 17:46:31', '2014-04-03 00:00:00'),
(21, NULL, 6, NULL, 0, 'Konferencja Naturalna i Bezpieczna Żywność', 'konferencja-naturalna-i-bezpieczna-zywnosc', '<p>\r\n	21 marca 2014 r. w Zielonym Dworku w Kraśniku odbyła się konferencja pod nazwą &quot;Naturalna i Bezpieczna Żywność - Europejska Marka Regionu Lubelskiego&quot; zorganizowana przez Uniwersytet Medyczny w Lublinie, Centrum Wdrożeń i Innowacji Uniwersytetu Medycznego oraz Starostwo Powiatowe w Kraśniku.</p>', '<p>\r\n	21 marca 2014 r. w Zielonym Dworku w Kraśniku odbyła się konferencja pod nazwą &quot;Naturalna i Bezpieczna Żywność - Europejska Marka Regionu Lubelskiego&quot; zorganizowana przez Uniwersytet Medyczny w Lublinie, Centrum Wdrożeń i Innowacji Uniwersytetu Medycznego oraz Starostwo Powiatowe w Kraśniku.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję uczestniczyć w konferencji w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	To świetna inicjatywa, wpisująca się doskonale w nowy kierunek rozwoju wojew&oacute;dztwa lubelskiego - oparty na nowoczesnej BIOGOSPODARCE.</p>\r\n<p>\r\n	BIOGOSPODARKA - to sektory, branże i przemysły związane z produkcją mebli, nawoz&oacute;w sztucznych, papieru, energii odnawialnej, zi&oacute;ł, bio-farmaceutyk&oacute;w, bio-produkt&oacute;w, środk&oacute;w ochrony roślin, bio-kosmetyk&oacute;w, paszy dla zwierząt, zagospodarowywaniem odpad&oacute;w i oczywiście - WYSOKIEJ JAKOŚCI, ZDROWEJ ŻYWNOŚCI, ŻYWNOŚCI FUNKCJONALNEJ.<br />\r\n	A nasz region ma olbrzymie zasoby naturalne, ludzkie i tradycje w tym obszarze gospodarczym. Na razie w niewielkim stopniu wykorzystane w nowoczesnej i innowacyjnej formie.</p>\r\n<p>\r\n	Działy bioekonomii - już teraz w woje&oacute;wdztwie lubelskim osiągają roczne przychody rzędu 12 mld zł (w tym ok. 3,5 mld zł w sektorze rolnym, leśnym i rybackim). 1 euro zainwestowane w nowoczesne sektory BIOGOSPODARKI wsparte badaniami i wdrożeniami naukowymi może wygenerować 10 euro wartości dodanej w sektorach biogospodarki.<br />\r\n	A w naszym regionie mamy bardzo wyrażną specjalizację naukową sektora B+R w zakresie nauk związanych z biogospodarką. Są możliwości zaangażowania około 60% potencjału naukowego regionu w proces budowy nowoczesnych sektor&oacute;w biogospodarki w regionie.</p>', 'porada-image-54073ce2e4e40.jpeg', 0, 'Konferencja Naturalna i Bezpieczna Żywność', 'Konferencja, Naturalna, Bezpieczna, Żywność, marca, 2014', '21 marca 2014 r.', '2014-09-03 18:08:02', '2014-09-03 18:49:23', '2014-03-21 00:00:00'),
(22, NULL, 5, NULL, 0, 'Konferencja podsumowująca kolejny etap realizacji projektu RSZZG', 'konferencja-podsumowujaca-kolejny-etap-realizacji-projektu-rszzg', '<p>\r\n	13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. &quot;Regionalny System Zarządzania Zmianą Gospodarczą&quot;.</p>', '<p>\r\n	13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. &quot;Regionalny System Zarządzania Zmianą Gospodarczą&quot;.</p>\r\n<p>\r\n	Słowo wstępne w ramach otwarcia konferencji wygłosił Pan Sławomir Sosnowski &ndash; Wicemarszałek Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Po wystąpieniu Wicemarszałka miałem przyjemność i satysfakcję zaprezentować gł&oacute;wne założenia projektu i jego gł&oacute;wne rezultaty w trakcie prezentacji nt. &bdquo;Realizacja projektu RSZZG w aspekcie przygotowania wojew&oacute;dztwa lubelskiego do wdrażania program&oacute;w operacyjnych nowej perspektywy finansowej UE na lata 2014-2020&rdquo;</p>\r\n<p>\r\n	Założeniem Regionalnego Systemu Zarządzania Zmianą Gospodarczą (RSZZG) jest integracja ludzi, informacji i podmiot&oacute;w zaangażowanych w kreowanie zmian społeczno-gospodarczych w spos&oacute;b umożliwiający dążenie do takiego modelu, w kt&oacute;rym podstawowe znaczenie ma wsp&oacute;łpraca r&oacute;żnorodnych środowisk regionalnych w ramach jednego systemu zarządzania zmianą gospodarczą, a perspektywie w ramach rozbudowy Regionalnego Systemu Innowacji.</p>\r\n<p>\r\n	REGIONALNY SYSTEM ZARZĄDZANIA ZMIANĄ GOSPODARCZĄ &ndash; to nowa jakość w zarządzaniu informacją gospodarczą w regionie.</p>\r\n<p>\r\n	W &bdquo;Założeniach systemu zarządzania rozwojem Polski&rdquo; opracowanych przez Ministerstwo Rozwoju Regionalnego stwierdzono, iż:</p>\r\n<p>\r\n	&bdquo;... Do niedawna sądzono powszechnie, że właściwe określanie cel&oacute;w rozwojowych i zapewnienie możliwości ich realizacji &ndash; jest jedynie funkcją dostępnych środk&oacute;w finansowych.</p>\r\n<p>\r\n	<strong>Utrzymujące się problemy, np. z:</strong></p>\r\n<p>\r\n	- budową autostrad w Polsce,<br />\r\n	- organizacją systemu wsp&oacute;łpracy sfery naukowo-badawczej z przemysłem,<br />\r\n	- alokacji środk&oacute;w UE na poziomie regionalnym, nie pozwalające na pełne wykorzystanie szansy na rozw&oacute;j regionu&oacute;w,</p>\r\n<p>\r\n	jednoznacznie wskazują, że ważnym, obok środk&oacute;w finansowych, a w dłuższej perspektywie &ndash; o wiele bardziej istotnym czynnikiem, decydującym o rozwoju danego regionu, jest funkcjonowanie sprawnego systemu zarządzania w sektorze publicznym.</p>\r\n<p>\r\n	<strong>Przykłady państw i region&oacute;w, kt&oacute;re w ostatnich kilku dekadach osiągnęły sukcesy gospodarcze wskazują, że obok przeprowadzania:</strong></p>\r\n<p>\r\n	- analiz dotyczących wyzwań,<br />\r\n	- budowania scenariuszy rozwojowych,<br />\r\n	- umiejętności wyznaczania cel&oacute;w strategicznych w dłuższych przedziałach czasowych,</p>\r\n<p>\r\n	<strong>istotne znaczenie dla powodzenia strategii rozwoju regionu mają:</strong></p>\r\n<p>\r\n	- umiejętności klasy politycznej,<br />\r\n	- jakość funkcjonowania systemu prawnego,<br />\r\n	- kultura administracyjna zakorzeniona w instytucjach państwa, a przede wszystkim<br />\r\n	- zdolność do bieżącego uczenia się i reagowania na zmiany w otoczeniu.</p>\r\n<p>\r\n	W państwach osiągających sukcesy gospodarcze dobrze funkcjonują systemy zarządzania procesami rozwojowymi!</p>\r\n<p>\r\n	<br />\r\n	Spos&oacute;b i jakość funkcjonowania systemu zarządzania procesami rozwojowymi w sektorze publicznym w całości oraz na poszczeg&oacute;lnych szczeblach zarządzania decyduje w dużej mierze o zdolności do odpowiedniego reagowania regionu (a tak naprawdę klasy politycznej / kadry zarządzającej i instytucji publicznych) na strategiczne wyzwania pojawiające się we wsp&oacute;łczesnym świecie.</p>\r\n<p>\r\n	Wyzwania te mają r&oacute;żny charakter i wynikają z r&oacute;żnorakich czynnik&oacute;w, mechanizm&oacute;w, zjawisk, proces&oacute;w, tendencji i trend&oacute;w, kt&oacute;rych do końca nie znamy &ndash; ale ich wsp&oacute;lną cechą jest jednak to, że mają one w coraz większym stopniu charakter ponadregionalny i ponadkrajowy oraz to, że stają się wsp&oacute;lne dla wielu lub niemal wszystkich mieszkańc&oacute;w Ziemi&rdquo;.</p>\r\n<p>\r\n	<strong>Najważniejszymi czynnikami i mechanizmami zmian gospodarki i rynku pracy w wymiarze regionalnym i globalnym w najbliższych latach, a może dekadach będą takie zjawiska, procesy, tendencje i trendy, jak:</strong></p>\r\n<p>\r\n	- Szybko rosnąca ilość wiedzy i informacji<br />\r\n	- Globalizacja<br />\r\n	- Rozw&oacute;j nowoczesnych technologii<br />\r\n	- Otwarty i wymagający rynek pracy<br />\r\n	- Rosnąca sieciowość i projektowość relacji<br />\r\n	- Postępująca digitalizacja<br />\r\n	- Nowy typ społeczeństwa<br />\r\n	- Oszczędzanie energii i odnawialność jej źr&oacute;deł<br />\r\n	- Dłuższe i lepsze życie<br />\r\n	- Zmiana struktury branż i sektor&oacute;w</p>\r\n<p>\r\n	<strong>Stąd też, w ramach realizacji projektu dochodzi to osiągania takich rezultat&oacute;w jak:</strong></p>\r\n<p>\r\n	- integrowanie wiedzy gospodarczej z najważniejszych źr&oacute;deł w jednym systemie</p>\r\n<p>\r\n	- planowanie i realizowanie badań i analiz na bazie realnych potrzeb informacyjnych<br />\r\n	[badania potrzeb informacyjnych],</p>\r\n<p>\r\n	- tworzony jest jednolity system informatyczny - jako narzędzie służące gromadzeniu, przetwarzaniu i upowszechnianiu informacji gospodarczych [system informatyczny projektu]</p>\r\n<p>\r\n	- na bieżącą dokonuje się monitoringu oraz dostarczania aktualnych informacji nt. zmian zachodzących w gospodarce regionu [system wskaźnik&oacute;w syntetycznych projektu]</p>\r\n<p>\r\n	- ma miejsce wsparcie analityczne procesu monitorowania i aktualizacji Strategii Rozwoju Wojew&oacute;dztwa Lubelskiego,</p>\r\n<p>\r\n	- ma miejsce wsparcie analityczne przygotowywania programu operacyjnego dla wojew&oacute;dztwa lubelskiego w ramach nowego okresu programowania w kontekście przyszłych instrument&oacute;w wsparcia przedsiębiorczości,</p>\r\n<p>\r\n	- tworzona jest baza danych do zasilania zasobami tematycznymi tworzonego Systemu Informacji Przestrzennej Lubelszczyzny.</p>', 'porada-image-5407451a1234e.jpeg', 0, 'Konferencja podsumowująca kolejny etap realizacji projektu RSZZG', 'Konferencja, podsumowująca, kolejny, etap, realizacji, projektu, RSZZG, grudnia, 2013, odbyła', '13 grudnia 2013 r. odbyła się konferencja poświęcona podsumowaniu bardzo ważnego etapu realizacji projektu, pt. "Regionalny System Zarz', '2014-09-03 18:43:05', '2014-09-03 18:48:48', '2013-12-13 00:00:00'),
(23, NULL, 5, NULL, 0, 'VII posiedzenie Rady Programowej Projektu RSZZG', 'vii-posiedzenie-rady-programowej-projektu-rszzg', '<p>\r\n	W dniu 6 grudnia 2013 r. w siedzibie Departamentu Regionalnego Programu Operacyjnego Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się VII posiedzenie Rady Programowej projektu systemowego Regionalny System Zarządzania Zmianą Gospodarczą</p>', '<p>\r\n	W dniu 6 grudnia 2013 r. w siedzibie Departamentu Regionalnego Programu Operacyjnego Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się VII posiedzenie Rady Programowej projektu systemowego Regionalny System Zarządzania Zmianą Gospodarczą.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować spotkanie oraz przedstawić informacje o aktualnie realizowanych zadaniach w ramach projektu RSZZG.</p>\r\n<p>\r\n	Podczas posiedzenia zostały przedstawione wyniki badania pogłębionego:</p>\r\n<p>\r\n	- &bdquo;Analiza podmiot&oacute;w oraz powiązań kooperacyjnych w sektorze rolno-spożywczym w kontekście zarządzania regionalnym łańcuchem dostaw żywności&quot; &ndash; dr Maciej Piotrowski, Quality Watch Sp. z o.o.</p>\r\n<p>\r\n	oraz wyniki badania dziedzinowego:</p>\r\n<p>\r\n	- &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; &ndash; dr Andrzej Tucki</p>', 'porada-image-54074622e88ce.jpeg', 0, 'VII posiedzenie Rady Programowej Projektu RSZZG', 'posiedzenie, Rady, Programowej, Projektu, RSZZG, dniu, grudnia, 2013', 'W dniu 6 grudnia 2013 r.', '2014-09-03 18:47:30', NULL, '2013-12-06 00:00:00'),
(24, NULL, 5, NULL, 0, 'Panel branżowy projektu RSZZG - Turystyka zdrowotna', 'panel-branzowy-projektu-rszzg-turystyka-zdrowotna', '<p>\r\n	4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn. &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; projektu Regionalny System Zarządzania Zmianą Gospodarczą.</p>', '<p>\r\n	4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn. &bdquo;Analiza możliwości rozwoju turystyki zdrowotnej w wojew&oacute;dztwie lubelskim w kontekście zidentyfikowanej inteligentnej specjalizacji regionu w dziedzinie usług medycznych i prozdrowotnych&quot; projektu Regionalny System Zarządzania Zmianą Gospodarczą.</p>\r\n<p>\r\n	W spotkaniu uczestniczyli przedstawiciele sektora turystyki, uczelni wyższych, instytucji otoczenia biznesu oraz administracji samorządowej.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować panel. W swoim wystąpieniu podkreśliłem ważną rolę rozwoju turystyki zdrowotnej w procesie wdrażania Regionalnej Strategii Innowacji. Zidentyfikowany potencjał do rozwoju turystyki zdrowotnej był m.in. jedną z przesłanek do wskazania usług medycznych i prozdrowotnych jako jednej z inteligentnych specjalizacji wojew&oacute;dztwa lubelskiego.</p>', 'porada-image-540747731f9d4.jpeg', 0, 'Panel branżowy projektu RSZZG - Turystyka zdrowotna', 'Panel, branżowy, projektu, RSZZG, Turystyka, zdrowotna, listopada, 2013, Trybunale, Koronnym', '4 listopada 2013 r. w Trybunale Koronnym w Lublinie odbył się organizowany przez Departament Gospodarki i Innowacji UMWL panel branżowy badania dziedzinowego pn.', '2014-09-03 18:53:06', NULL, '2013-11-04 00:00:00'),
(25, NULL, 7, NULL, 0, 'Konferencja "Stypendia naukowe dla doktorantów II"', 'konferencja-stypendia-naukowe-dla-doktorantow-ii', '<p>\r\n	Projekt jest realizowany od 2010 r. w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego.</p>', '<p>\r\n	W dniu 11 grudnia 2013 r. odbyła się w Sali Błękitnej Lubelskiego Urzędu Wojew&oacute;dzkiego w Lublinie konferencja podsumowującą projekt &bdquo;Stypendia naukowe dla doktorant&oacute;w II&quot; w ramach Programu Operacyjnego Kapitał Ludzki.</p>\r\n<p>\r\n	Projekt jest realizowany od 2010 r. w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Podczas konferencji Krzysztof Grabczuk - wicemarszałek Wojew&oacute;dztwa Lubelskiego wręczył dyplomy 94 doktorantom, kt&oacute;rzy otrzymali stypendia w ramach III i IV naboru wniosk&oacute;w odpowiednio w 2012 r. i w 2013 r.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję zaprezentować gł&oacute;wne założenia i rezultaty projektu.</p>\r\n<p>\r\n	Następnie sześciu doktorant&oacute;w zaprezentowało prezentacje wynik&oacute;w badań naukowych swoich prac doktorskich. Cztery prezentacje doktorat&oacute;w zostały wzbogacone o wystąpienie przedsiębiorcy/jednostki z sektora B+R, z kt&oacute;rymi doktoranci nawiązali wsp&oacute;łpracę w zakresie wdrażania wynik&oacute;w badań naukowych.</p>\r\n<p>\r\n	Celem projektu &bdquo;Stypendia naukowe dla doktorant&oacute;w II&quot; jest wsparcie proces&oacute;w innowacyjnych w regionalnej gospodarce poprzez wyasygnowanie stypendi&oacute;w naukowych dla 212 doktorant&oacute;w kształcących się na kierunkach przyczyniających się do wzmocnienia konkurencyjności i rozwoju gospodarczego Lubelszczyzny, uwzględnionych w Regionalnej Strategii Innowacji Wojew&oacute;dztwa Lubelskiego.</p>', 'porada-image-54084bd47ad06.jpeg', 0, 'Konferencja "Stypendia naukowe dla doktorantów II"', 'Konferencja, "Stypendia, naukowe, doktorantów, dniu, grudnia, 2013, odbyła, się, Sali', 'W dniu 11 grudnia 2013 r. odbyła się w Sali Błękitnej Lubelskiego Urzędu Wojew', '2014-09-04 13:24:03', NULL, '2013-12-11 00:00:00'),
(26, NULL, 7, NULL, 0, 'REGIONALNE FORUM GOSPODARCZE W KRAŚNIKU - INNOWACJE I WSPÓŁPRACA', 'regionalne-forum-gospodarcze-w-krasniku-innowacje-i-wspolpraca', '<p>\r\n	Lubelskiego w Lublinie we wsp&oacute;łpracy z przedsiębiorcami z Kraśnickiej Izby Gospodarczej i pracownikami Starostwa Powiatowego w Kraśniku zorganizowali &quot;REGIONALNE FORUM GOSPODARCZE INNOWACJE I WSP&Oacute;ŁPRACA &ndash; NOWY WYMIAR ROZWOJU REGIONU&quot; - z udziałem kilkuset kraśnickich i lubelskich przedsiębiorc&oacute;w i samorządowc&oacute;w, przedstawicieli świata nauki oraz ekonomist&oacute;w.</p>', '<p>\r\n	26 listopada 2013 r. w Kraśniku pracownicy Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie we wsp&oacute;łpracy z przedsiębiorcami z Kraśnickiej Izby Gospodarczej i pracownikami Starostwa Powiatowego w Kraśniku zorganizowali &quot;REGIONALNE FORUM GOSPODARCZE INNOWACJE I WSP&Oacute;ŁPRACA &ndash; NOWY WYMIAR ROZWOJU REGIONU&quot; - z udziałem kilkuset kraśnickich i lubelskich przedsiębiorc&oacute;w i samorządowc&oacute;w, przedstawicieli świata nauki oraz ekonomist&oacute;w.</p>\r\n<p>\r\n	Gł&oacute;wnym tematem debaty były szanse na rozw&oacute;j lokalnej przedsiębiorczości w nowej perspektywie finansowej UE na lata 2014 &ndash; 2020 w wojew&oacute;dzwie lubelskim i powiecie kraśnickim.</p>\r\n<p>\r\n	To pierwsze tego typu wydarzenie gospodarcze w historii powiatu krasnickiego i miasta Kraśnik.</p>\r\n<p>\r\n	Forum gospodarcze to doskonała platforma wymiany informacji, nawiązywania kontakt&oacute;w gospodarczych i promocji firm oraz samorząd&oacute;w. I świetnie, że z tych możliwości skorzystało kilkaset os&oacute;b przedstawicieli firm, organizacji okołobiznesowych, świata nauki i samorząd&oacute;w.</p>\r\n<p>\r\n	Z pewnością to wydarzenie powinno być kontynuowane w kolejnych latach i na stałe wpisać się w kalendarz najważniejszych wydarzeń gospodarczych w woje&oacute;wdztwie lubelskim i powiecie kraśnickim.</p>', 'porada-image-54084c8a71c74.jpeg', 0, 'REGIONALNE FORUM GOSPODARCZE W KRAŚNIKU - INNOWACJE I WSPÓŁPRACA', 'REGIONALNE, FORUM, GOSPODARCZE, KRAŚNIKU, INNOWACJE, WSPÓŁPRACA, listopada, 2013', '26 listopada 2013 r.', '2014-09-04 13:27:05', NULL, '2013-11-26 00:00:00'),
(27, NULL, 4, NULL, 0, 'Konferencja naukowa - Dziedzictwo Jana Pawła II', 'konferencja-naukowa-dziedzictwo-jana-pawla-ii', '<p>\r\n	8 kwietnia 2014 r. z olbrzymią radością i satysfakcją wziąłem udział w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Pana Krzysztofa Hetmana w konferencji naukowej poświęconej dziedzictwu Jana Pawła II zorganizowanej przez Katolicki Uniwersytet Lubelski, Starostwo Powiatowe w Kraśniku i Powiatowe Centrum Pomocy Rodzinie pn.&quot;Dziedzictwo Jana Pawła II. Przesłanie dla Polski&quot;.</p>', '<p>\r\n	8 kwietnia 2014 r. z olbrzymią radością i satysfakcją wziąłem udział w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Pana Krzysztofa Hetmana w konferencji naukowej poświęconej dziedzictwu Jana Pawła II zorganizowanej przez Katolicki Uniwersytet Lubelski, Starostwo Powiatowe w Kraśniku i Powiatowe Centrum Pomocy Rodzinie pn.&quot;Dziedzictwo Jana Pawła II. Przesłanie dla Polski&quot;.</p>\r\n<p>\r\n	Niezwykle bogaty program konferencji uzmysławiał wszystkim obecnym na konferencji jak wielką spuściznę pozostawił po sobie Papież Polak. Zakres temetyczny dzieł teologicznych oraz prac o wydźwięku społeczno-moralnym, jakie pozostawił Jan Paweł II, jest niezwykle szeroki. Pozwala to czerpać nauki z tego dorobku nie tylko osobom duchowym, ale przede wszystkim osobom świeckim.</p>\r\n<p>\r\n	Pontyfikat Jana Pawła II przypadł na niezwykle ważny i przełomowy okres dla Polski, Europy i całego świata. Nie spos&oacute;b nie zauważyć, że kształtował on ten okres historii w szeg&oacute;lności naszego kraju i Europy. A tematyka konferencji dowodzi, jak szerokie było spektrum zainteresowań wsp&oacute;łczesnością bł. Jana Pawła II. Żaden z istotnych problem&oacute;w, z jakimi zmagał się świat, nie uszedł jego uwadze. Z tym większą troską należy pochylać się nad tą spuścizną, odkrywając w niej coraz nowe przesłania dla przyszłości.</p>\r\n<p>\r\n	Prelegenci, moderatorzy dyskusji panelowych oraz dyskutanci starali się odkrywać przesłania i nauki Jana Pawła II dla obecnych i przyszłych pokoleń w wymiarach: wychowania, roli rodziny, demokracji, ochrony zdrowia i wielu innych.</p>\r\n<p>\r\n	Z pewnością referaty i wnioski z konferencji zaowocują nowymi odkryciami i bogatym plonem dyskusji.</p>', 'porada-image-54084dab3f507.jpeg', 0, 'Konferencja naukowa - Dziedzictwo Jana Pawła II', 'Konferencja, naukowa, Dziedzictwo, Jana, Pawła, kwietnia, 2014', '8 kwietnia 2014 r.', '2014-09-04 13:31:54', NULL, '2014-04-08 00:00:00'),
(28, NULL, 4, NULL, 0, 'Dzień Patrona w Zespole Szkół Nr 3 w Kraśniku', 'dzien-patrona-w-zespole-szkol-nr-3-w-krasniku', '<p>\r\n	4 kwietnia 2014 r. z olbrzymią przyjemnością i satysfakcją uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w uroczystościach Dnia Patrona Zespołu Szk&oacute;ł Nr 3 w Kraśniku Juliusza Słowackiego połączonych ze ślubowaniem pierwszych klas mundurowych.</p>', '<p>\r\n	4 kwietnia 2014 r. z olbrzymią przyjemnością i satysfakcją uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w uroczystościach Dnia Patrona Zespołu Szk&oacute;ł Nr 3 w Kraśniku Juliusza Słowackiego połączonych ze ślubowaniem pierwszych klas mundurowych.</p>', 'porada-image-54084ecb80f7f.jpeg', 0, 'Dzień Patrona w Zespole Szkół Nr 3 w Kraśniku', 'Dzień, Patrona, Zespole, Szkół, Kraśniku, kwietnia, 2014', '4 kwietnia 2014 r.', '2014-09-04 13:36:42', NULL, '2014-04-04 00:00:00'),
(29, NULL, 4, NULL, 0, 'Wojewódzki Turniej Wiedzy o Janie Pawle II w Kraśniku', 'wojewodzki-turniej-wiedzy-o-janie-pawle-ii-w-krasniku', '<p>\r\n	2 kwietnia 2014 r. z olbrzymią radością uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w &quot;Wojew&oacute;dzkim Turnieju Wiedzy o Janie Pawle II - Miejsce dla Każdego&quot; - zorganizowanym już po raz dziewiąty w kraśnickim gimnazjum nr 2.</p>', '<p>\r\n	2 kwietnia 2014 r. z olbrzymią radością uczestniczyłem w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego w &quot;Wojew&oacute;dzkim Turnieju Wiedzy o Janie Pawle II - Miejsce dla Każdego&quot; - zorganizowanym już po raz dziewiąty w kraśnickim gimnazjum nr 2.</p>\r\n<p>\r\n	Do udziału w konkursie zgłosiło się kilkudziesięciu uczni&oacute;w gimnazj&oacute;w i lice&oacute;w m.in. z Kraśnika, Wilkołaza, Polichny i Krasnegostawu.</p>\r\n<p>\r\n	Najlepiej zaprezentowało się gimnazjum z Wilkołaza. Na drugim miejscu znalazło się Publiczne Gimnazjum nr 2 z Kraśnika, a na trzecim gimnazjaliści z Mętowa.</p>\r\n<p>\r\n	Turniejowi wiedzy towarzyszyła wystawa rzeźbiarska Szczepana Ignaczyńskiego pod nazwą &quot;Kapliczki Polskie&quot;.</p>', 'porada-image-54084f5ac27f4.jpeg', 0, 'Wojewódzki Turniej Wiedzy o Janie Pawle II w Kraśniku', 'Wojewódzki, Turniej, Wiedzy, Janie, Pawle, Kraśniku, kwietnia, 2014', '2 kwietnia 2014 r.', '2014-09-04 13:39:06', NULL, '2014-04-02 00:00:00'),
(30, NULL, 3, NULL, 0, 'Otwarcie nowoczesnego bloku operacyjnego w kraśnickim szpitalu', 'otwarcie-nowoczesnego-bloku-operacyjnego-w-krasnickim-szpitalu', '<p>\r\n	Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca 2014 r.</p>', '<p>\r\n	Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca 2014 r. W uroczystościach wzięli udział m.in. wiceminister zdrowia Aleksander Sopliński, dyrektor lubelskiego oddziału wojew&oacute;dzkiego NFZ Krzysztof Tuczapski, marszałek wojew&oacute;dztwa lubelskiego Krzysztof Hetman, poseł na sejm RP Jan Łopata, starosta kraśnicki Andrzej Maj oraz przedstawiciele władz samorządowych powiatu kraśnickiego.</p>\r\n<p>\r\n	Na początku grudnia 2012 r. dyrektor SP ZOZ w Kraśniku Marek Kos podpisał w Urzędzie Marszałkowskim umowę na realizację inwestycji kluczowej dla funkcjonowania plac&oacute;wki. Chodzi o przebudowę bloku operacyjnego, na kt&oacute;rą pozyskano unijne dofinansowanie. Całkowita wartość tego projektu to ponad 6 mln zł.</p>', 'porada-image-5408567a8b835.jpeg', 0, 'Otwarcie nowoczesnego bloku operacyjnego w kraśnickim szpitalu', 'Otwarcie, nowoczesnego, bloku, operacyjnego, kraśnickim, szpitalu, Oficjalne, otwarcie, odbyło, się', 'Oficjalne otwarcie nowoczesnego bloku operacyjnego odbyło się 25 marca br. W uroczystościach wzięli udział m.in.', '2014-09-04 14:09:29', '2014-09-04 14:16:06', '2014-03-25 00:00:00'),
(31, NULL, 3, NULL, 0, 'Nowa komisja stała przy Radzie Powiatu Kraśnickiego', 'nowa-komisja-stala-przy-radzie-powiatu-krasnickiego', '<p>\r\n	27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali nową komisję - Komisję Rozwoju Gospodarczego. W jej skład weszli wszyscy radni, kt&oacute;rych kandydatury zostały zgłoszone w trakcie sesji, tj.: Piotr Janczarek, Wiesław Kołczewski, Piotr Nowaczek, Agnieszka Orzeł-Depta, Bożena Kozub, Karol Rej, Wiesław Marzec, Roman Bijak.</p>', '<p>\r\n	27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali nową komisję - Komisję Rozwoju Gospodarczego. W jej skład weszli wszyscy radni, kt&oacute;rych kandydatury zostały zgłoszone w trakcie sesji, tj.: Piotr Janczarek, Wiesław Kołczewski, Piotr Nowaczek, Agnieszka Orzeł-Depta, Bożena Kozub, Karol Rej, Wiesław Marzec, Roman Bijak.</p>\r\n<p>\r\n	Zostałem wybrany na przewodniczącego Komisji. Dziękuję wszystkim Radnym za inicjatywę idącą w kierunku większego zaangażowania w sprawy gospodarcze powiatu oraz zaufanie i powierzenie mi funkcji Przewodniczącego Komisji.</p>\r\n<p>\r\n	Po 2014 roku przed samorządami lokalnymi z terenu wojew&oacute;dztwa lubelskiego otworzą się nowe możliwości i perspektywy rozwoju społeczno-gospodarczego oraz nowe wyzwania i obowiązki.</p>\r\n<p>\r\n	Będą one:</p>\r\n<p>\r\n	1) z jednej strony związane z możliwością włączenia się w realizację ambitnego planu rozwoju gospodarczego Samorządu Wojew&oacute;dztwa Lubelskiego do 2020 r. zapisanego w zaktualizowanej Strategii Rozwoju Wojew&oacute;dztwa Lubelskiego i Regionalnej Strategii Innowacji - opartego na wsparciu rozwoju przedsiębiorczości, wiedzy i nowych technologiach, energetyce niskoemisyjnej (w tym OZE), a także na restrukturyzacji i modernizacji sektora rolnego,</p>\r\n<p>\r\n	2) a z drugiej strony z uruchamianiem nowej perspektywy funduszy unijnych na lata 2014-2020, w ramach kt&oacute;rej będzie możliwość realizacji wielu ważnych projekt&oacute;w prorozwojowych dla powiatu ze wsparciem funduszy UE.</p>\r\n<p>\r\n	Samorządy lokalne: gminne i powiatowe muszą być zatem bardzo dobrze przygotowane programowo i organizacyjnie do realizacji nowych przedsięwzięć i inwestycji w formie nowych typ&oacute;w projekt&oacute;w, z kt&oacute;rych większość powinna być realizowana w partnerstwach/konsorcjach z innymi samorządami, jednostkami naukowymi, badawczo-rozwojowymi i przedsiębiorstwami.</p>\r\n<p>\r\n	Moim zdaniem Komisja Rozwoju Gospodarczego powinna skupiać się przede wszystkim nad inicjowaniem, przygotowywaniem, opiniowaniem i promowaniem spraw, temat&oacute;w, dokument&oacute;w strategicznych i uchwał gospodarczych w takich obszarach rozwoju społeczno-gospodarczego jak:</p>\r\n<p>\r\n	1. Realizacja inwestycji i projekt&oacute;w prorozwojowych:</p>\r\n<p>\r\n	- projekty mające wymiar i charakter projekt&oacute;w zintegrowanych, kluczowych, systemowych, partnerskich, konsorcjalnych, w formule PPP (projekty innowacyjne, edukacyjne i naukowe o dużym zasięgu oddziaływania, B+R, z zakresu rozwoju społeczeństwa cyfrowego, promocji gospodarczej i marketingu gospodarczego, tworzenia i rozwoju stref aktywności gospodarczej i teren&oacute;w inwestycyjnych itp.),<br />\r\n	- projekty generujące dużą wartość dodaną, nowe miejsca pracy,</p>\r\n<p>\r\n	2. Wspieranie rozwoju przedsiębiorczości lokalnej:</p>\r\n<p>\r\n	- prowadzenie przemyślanej i zorganizowanej (systemowej) polityki wobec przedsiębiorc&oacute;w, potencjalnych inwestor&oacute;w, turyst&oacute;w (organizacja starostwa powiatowego i urzęd&oacute;w gmin w oparciu o zasadę kompleksowej obsługi interesanta &ndash; klienta),<br />\r\n	- powoływanie i/lub wspieranie instytucji pobudzających inicjatywy gospodarcze, towarzystwa i izby gospodarcze, instytucje wspierania inicjatyw, agencje rozwoju lokalnego i regionalnego,<br />\r\n	- tworzenie i rozw&oacute;j stref aktywności gospodarczej,<br />\r\n	- budowa i dbałość o wysoki pozom infrastruktury technicznej (drogi, oczyszczanie ściek&oacute;w, zagospodarowanie odpad&oacute;w, kanalizacja, sieci internetowe, itp.),<br />\r\n	- tworzenie funduszy poręczeń, inkubator&oacute;w przedsiębiorczości,<br />\r\n	- wsparcie międzynarodowej ekspansji przedsiębiorstw,<br />\r\n	- zapewnienie r&oacute;żnorodnych źr&oacute;deł finansowania inwestycji,<br />\r\n	- stymulowanie wsp&oacute;łpracy branżowej,<br />\r\n	- wspieranie powstawania i rozwoju lokalnych i regionalnych klastr&oacute;w gospodarczych,<br />\r\n	- ukierunkowywanie systemu edukacyjnego na specjalizacje regionu,<br />\r\n	- inwestowanie w kapitał ludzki,<br />\r\n	- uczestnictwo w budowie struktur regionalnego systemu innowacji,<br />\r\n	- budowanie partnerstw lokalnych,<br />\r\n	- budowanie i promocja marki gospodarczej powiatu/regionu.</p>\r\n<p>\r\n	3. Pozyskiwanie/przyciąganie inwestycji z kapitałem zagranicznym:</p>\r\n<p>\r\n	- w sektorach i branżach nie stanowiących istotnej konkurencji dla podmiot&oacute;w gospodarczych obecnych w powiecie kraśnickim i regionie lubelskim.</p>\r\n<p>\r\n	4. Aktywizacja rynku pracy i instytucji edukacyjnych oraz wsp&oacute;łpraca z jednostkami B+R:</p>\r\n<p>\r\n	- ukierunkowywanie systemu edukacyjnego na rynek pracy i specjalizacje regionu,<br />\r\n	- inwestowanie w kapitał intelektualny,<br />\r\n	- podejmowanie wsp&oacute;łpracy z uczelniami wyższymi, jednostkami naukowymi<br />\r\n	i badawczo-rozwojowymi z terenu wojew&oacute;dztwa lubelskiego,<br />\r\n	- inicjowanie i realizacja projekt&oacute;w systemowych z dofinansowaniem ze środk&oacute;w EFS.</p>\r\n<p>\r\n	5. Planowanie strategiczne:</p>\r\n<p>\r\n	- wsp&oacute;łpraca przy opracowywaniu i opiniowanie projekt&oacute;w nowych dokument&oacute;w strategicznych, planistycznych i programowych na lata 2014-2020.</p>\r\n<p>\r\n	Na pierwszym merytorycznym posiedzeniu Komisji Rozwoju Gospodarczego zaplanowanym na 3 grudnia będziemy dyskutowali nad założeniami Ramowego Planu Pracy na 2014 rok.</p>', 'porada-image-5408593b2170c.jpeg', 0, 'Nowa komisja stała przy Radzie Powiatu Kraśnickiego', 'Nowa, komisja, stała, przy, Radzie, Powiatu, Kraśnickiego, listopada, 2013, ostatniej', '27 listopada 2013 r. na ostatniej sesji Rady Powiatu Kraśnickiego Radni powołali now', '2014-09-04 14:21:14', NULL, '2013-11-27 00:00:00'),
(32, NULL, 1, NULL, 0, 'Konferencja "Biogospodarka Powiatu Biłgorajskiego"', 'konferencja-biogospodarka-powiatu-bilgorajskiego', '<p>\r\n	21 października miałem olbrzymią przyjemość i satysfakcję w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego gościć w powiecie biłgorajskim i wziąć udział w konferencji pt. &quot;Biogospodarka Powiatu Biłgorajskiego w nowej perspektywie finansowej 2014-2020&quot;.</p>', '<p>\r\n	21 października miałem olbrzymią przyjemość i satysfakcję w imieniu Pana Krzysztofa Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego gościć w powiecie biłgorajskim i wziąć udział w konferencji pt. &quot;Biogospodarka Powiatu Biłgorajskiego w nowej perspektywie finansowej 2014-2020&quot;.</p>\r\n<p>\r\n	&nbsp;</p>', 'porada-image-54085d598eb61.jpeg', 0, 'Konferencja "Biogospodarka Powiatu Biłgorajskiego"', 'Konferencja, "Biogospodarka, Powiatu, Biłgorajskiego", października, miałem, olbrzymi', '21 października miałem olbrzymi', '2014-09-04 14:38:48', NULL, '2013-10-21 00:00:00'),
(33, NULL, 1, NULL, 0, 'Innowacyjne Techn. w Inżynierii i Kształtowaniu Środowiska', 'innowacyjne-techn-w-inzynierii-i-ksztaltowaniu-srodowiska', '<p>\r\n	27 czerwca 2013 r. miałem przyjemność i satysfakcję uczestniczyć w imieniu Pana Krzysztof Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego na konferencji naukowo-technicznej - &quot;Innowacyjne Technologie w Inżynierii i Kształtowaniu Środowiska&quot; na Uniwersytecie Przyrodniczym w Lublinie.</p>', '<p>\r\n	27 czerwca 2013 r. miałem przyjemność i satysfakcję uczestniczyć w imieniu Pana Krzysztof Hetmana - Marszałka Wojew&oacute;dztwa Lubelskiego na konferencji naukowo-technicznej - &quot;Innowacyjne Technologie w Inżynierii i Kształtowaniu Środowiska&quot; na Uniwersytecie Przyrodniczym w Lublinie.</p>\r\n<p>\r\n	Konferencja odbywała się w dniach 27 - 29 czerwca 2013 r. i była zorganizowana pod honorowym patronatem JM Rektora Uniwersytetu Przyrodniczego w Lublinie oraz Marszałka Wojew&oacute;dztwa Lubelskiego.</p>\r\n<p>\r\n	Celem konferencji było zaprezentowanie innowacyjnych technologii w zakresie gospodarki odpadami, degradacji i rekultywacji gleb, gospodarki wodno &ndash; ściekowej, a także zr&oacute;wnoważonego rozwoju i kształtowania środowiska.</p>\r\n<p>\r\n	Miałem przyjemność i satysfakcję w imieniu Marszałka Wojew&oacute;dztwa Lubelskiego - Krzysztofa Hetmana otworzyć konferencję wraz z JM Rektorem Uniwersytetu Przyrodniczego - prof. dr hab. Marianem Wesołowskim.</p>\r\n<p>\r\n	Następnie wygłosiłem prezentację nt. &bdquo;Działania Samorządu Wojew&oacute;dztwa Lubelskiego w zakresie wspierania rozwoju OZE w regionie. Inteligentne Specjalizacje Regionu&rdquo;.</p>', 'porada-image-54085e0e94d20.jpeg', 0, 'Innowacyjne Techn. w Inżynierii i Kształtowaniu Środowiska', 'Innowacyjne, Techn., Inżynierii, Kształtowaniu, Środowiska, czerwca, 2013', '27 czerwca 2013 r.', '2014-09-04 14:41:49', NULL, '2013-06-27 00:00:00'),
(34, NULL, 1, NULL, 0, 'V Wschodnie Forum Gospodarcze Lub-Invest', 'v-wschodnie-forum-gospodarcze-lub-invest', '<p>\r\n	W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ&oacute;w Lubelskich S.A. w Lublinie odbyło się V Wschodnie Forum Gospodarcze Lub &ndash; Invest. W spotkaniu uczestniczyli przedsiębiorcy, samorządowcy i ambasadorzy, kt&oacute;rzy rozmawiali o wsp&oacute;łpracy w ramach obszaru przygranicznego i rozwoju biznesu we wschodniej części Europy.</p>', '<p>\r\n	W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ&oacute;w Lubelskich S.A. w Lublinie odbyło się V Wschodnie Forum Gospodarcze Lub &ndash; Invest. W spotkaniu uczestniczyli przedsiębiorcy, samorządowcy i ambasadorzy, kt&oacute;rzy rozmawiali o wsp&oacute;łpracy w ramach obszaru przygranicznego i rozwoju biznesu we wschodniej części Europy.</p>\r\n<p>\r\n	W ramach programu Forum jednym z blok&oacute;w tematycznych był panel - &bdquo;Możliwości rozwoju fotowoltaiki w wojew&oacute;dztwie lubelskim.&rdquo;</p>\r\n<p>\r\n	W programie wzięli udział przedstawiciele ministerstwa gospodarki, nauki, biznesu i samorząd&oacute;w.</p>\r\n<p>\r\n	Spotkanie było doskonałą okazją do przekazania informacji i wymiany pogląd&oacute;w w zakresie perspektywy rozwoju energetyki odnawialnej a gł&oacute;wnie fotowoltaiki.</p>\r\n<p>\r\n	Informacje TV KRAŚNIK:&nbsp;<a href="http://www.ktv.com.pl/glowna/?f=2076#skip_content">http://www.ktv.com.pl/glowna/?f=2076#skip_content</a></p>', 'porada-image-54085f29cbbfa.jpeg', 0, 'V Wschodnie Forum Gospodarcze Lub-Invest', 'Wschodnie, Forum, Gospodarcze, Lub-Invest, dniach, 20&ndash;21, czerwca, 2013, terenie, Międzynarodowych', 'W dniach 20&ndash;21 czerwca 2013 r. na terenie Międzynarodowych Targ', '2014-09-04 14:46:33', '2014-09-04 14:53:25', '2013-06-21 00:00:00'),
(35, NULL, 2, NULL, 0, 'Wykluczenie cyfrowe I - Spotkanie z partnerami projektu', 'wykluczenie-cyfrowe-i-spotkanie-z-partnerami-projektu', '<p>\r\n	17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim (edycja I)&quot;.</p>', '<p>\r\n	17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim (edycja I)&quot;.</p>\r\n<p>\r\n	Gł&oacute;wnym celem spotkania było roczne podsumowanie funkcjonowania sprzętu komputerowego i Internetu u Beneficjent&oacute;w Ostatecznych.</p>\r\n<p>\r\n	W spotkaniu wzięli udział przedstawiciele firm: z ramienia firmy Talex S.A. prezentację przedstawił Pan Piotr Jaszczak - Dyrektor ds. Usług Wsparcia i Projekt&oacute;w IT, Firmę ENECO reprezentował Pan Artur Komorowski. Z ramienia Lidera projektu prezentację przedstawił Pan Krzysztof Ławnik - Koordynator projektu. Zostały om&oacute;wione takie zagadnienia jak: inwentaryzacja sprzętu przeprowadzona na koniec roku, zwiększenie liczby Beneficjent&oacute;w Ostatecznych, awarie zgłaszane do Talex-u i Nordisk-u i związane z tym problemy, transfery danych. Przedstawiono r&oacute;wnież najbliższe działania w Projekcie, kt&oacute;rymi będą: monitoring i ankietyzacja Beneficjent&oacute;w Ostatecznych, przetarg na wyłonienie wykonawc&oacute;w na dostawę zestaw&oacute;w komputerowych, usługę dostępu do Internetu i szkolenie, weryfikacja jednostek podległych, dystrybucja materiał&oacute;w promocyjnych przekazanych przez Władzę Wdrażającą Programy Europejskie.</p>\r\n<p>\r\n	<br />\r\n	Projekt &quot;Przeciwdziałanie wykluczeniu cyfrowemu w wojew&oacute;dztwie lubelskim&quot; jest realizowany dzięki wsp&oacute;łfinansowaniu przez Unię Europejską w ramach Europejskiego Funduszu Rozwoju Regionalnego realizowany w ramach Programu Operacyjnego Innowacyjna Gospodarka na lata 2007-2013, Oś Priorytetowa 8 Społeczeństwo Informacyjne - zwiększanie Innowacyjności Gospodarki, Działanie 8.3 Przeciwdziałanie wykluczeniu cyfrowemu - eInclusion.</p>\r\n<p>\r\n	Całkowita wartość projektu: 18 445 445,00 PLN</p>\r\n<p>\r\n	Wartość dofinansowania: 18 445 445,00 PLN</p>\r\n<p>\r\n	Projekt jest realizowany od 2010 roku w ramach konsorcjum Wojew&oacute;dztwa Lubelskiego, 47 Gmin i dw&oacute;ch Powiat&oacute;w wojew&oacute;dztwa lubelskiego.</p>\r\n<p>\r\n	Gł&oacute;wnym celem projektu jest zapewnienie bezpłatnego dostępu do Internetu oraz niezbędnych urządzeń i oprogramowania dla 2795 gospodarstw domowych z terenu 47 Gmin i 2 Powiat&oacute;w wojew&oacute;dztwa lubelskiego, zagrożonych wykluczeniem z aktywnego uczestnictwa w społeczeństwie informacyjnym, ze względu na trudną sytuację materialną lub niepełnosprawność. Dodatkowo w sprzęt komputerowy i Internet zostaną wyposażone instytucje podległe JST (306 zestaw&oacute;w komputerowych i 81 podłączeń do Internetu).</p>\r\n<p>\r\n	Udział w Projekcie jest bezpłatny. Koszty uczestnictwa w Projekcie pokrywane są z Europejskiego Funduszu Rozwoju Regionalnego (85%) i Budżetu Państwa (15%).</p>\r\n<p>\r\n	Zasięg/Partnerzy Projektu:</p>\r\n<p>\r\n	Gmina Adam&oacute;w, Gmina i Miasto Annopol, Gmina Borki, Gmina Cyc&oacute;w, Gmina Czemierniki, Gmina Dębowa Kłoda, Gmina Dubienka, Gmina Goraj, Gmina Hańsk, Gmina Hrubiesz&oacute;w, Gmina Jeziorzany, Gmina Kodeń, Gmina Konopnica, Gmina Konstantyn&oacute;w, Gmina i Miasto Krasnobr&oacute;d, Gmina Krzywda, Gmina Kur&oacute;w, Gmina Leśna Podlaska, Miasto Lublin, Gmina Łabunie, Gmina Łomazy, Gmina Miączyn, Miasto Międzyrzec Podlaski, Gmina Międzyrzec Podlaski, Gmina Milan&oacute;w, Gmina Mircze, Gmina Niedrzwica Duża, Gmina Piszczac, Gmina Podedw&oacute;rze, Miasto Puławy, Gmina Radzyń Podlaski, Miasto Rejowiec Fabryczny, Gmina Rejowiec Fabryczny, Gmina Sawin, Gmina Siedliszcze, Gmina Sosnowica, Gmina Spiczyn, Gmina Stanin, Gmina Stary Brus, Gmina Telatyn, Gmina Trzydnik Duży, Gmina Tuczna, Gmina Wierzbica, Miasto Włodawa, Gmina Wola Uhruska, Gmina W&oacute;lka, Gmina Żyrzyn, Powiat Puławski, Powiat Świdnicki.</p>\r\n<p>\r\n	Projekt będzie realizowany do 2015 roku.</p>', 'porada-image-54086175720ed.jpeg', 0, 'Wykluczenie cyfrowe I - Spotkanie z partnerami projektu', 'Wykluczenie, cyfrowe, Spotkanie, partnerami, projektu, grudnia, 2013, uczestniczyłem, spotkaniu, Projektu', '17 grudnia 2013 r. uczestniczyłem w spotkaniu z partnerami Projektu "Przeciwdziałanie wykluczeniu cyfrowemu w wojew', '2014-09-04 14:56:20', NULL, '2013-12-17 00:00:00'),
(36, NULL, 2, NULL, 0, '2908 km sieci szerokopasmowej na terenie województwa lubelskiego', '2908-km-sieci-szerokopasmowej-na-terenie-wojewodztwa-lubelskiego', '<p>\r\n	<span style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 13.3333339691162px; line-height: 18px; background-color: rgb(255, 255, 255);">22 maja 2013 r. miałem przyjemność i satysfakcję uczestniczyć w uroczystości podpisania umowy na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, realizowanego w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.&nbsp;</span><br style="color: rgb(20, 24, 35); font-family: \'lucida grande\', tahoma, verdana, arial, sans-serif; font-size: 13.3333339691162px; line-height: 18px; background-color: rgb(255, 255, 255);" />\r\n	&nbsp;</p>', '<p>\r\n	22 maja 2013 r. miałem przyjemność i satysfakcję uczestniczyć w uroczystości podpisania umowy na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, realizowanego w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.</p>\r\n<p>\r\n	W podpisaniu umowy w Lubelskim Parku Naukowo-Technologicznym S.A. wiął udział marszałek wojew&oacute;dztwa lubelskiego Krzysztof Hetman i wicemarszałek Sławomir Sosnowski, kt&oacute;rzy podpisali umowę na realizację projektu &quot;Sieć Szerokopasmowa Polski Wschodniej - wojew&oacute;dztwo lubelskie&quot;, z wykonawcą firmą Aeronaval de Construcciones e Instalaciones z Hiszpanii.</p>\r\n<p>\r\n	Jest to projekt o znaczeniu cywilizacyjnym dla naszego regionu. W historii samorządu wojew&oacute;dztwa nie było jeszcze inwestycji realizowanej na tak dużą skalę. W wojew&oacute;dztwie lubelskim powstanie 2 908 km sieci oraz 312 węzł&oacute;w do dalszej dystrybucji Internetu. Sieć szerokopasmowa będzie przebiegała przez 198 jednostek samorządu terytorialnego.</p>\r\n<p>\r\n	Dzięki wybudowanej sieci Internetowej w zasięgu sieci szerokopasmowej znajdzie się do 95 proc. gospodarstw domowych wojew&oacute;dztwa lubelskiego. W ramach projektu, w zakresie praktycznego wykorzystania Internetu oraz technik cyfrowych, przeszkolonych zostanie ponad 2 tysiące os&oacute;b.</p>\r\n<p>\r\n	Wartość całego projektu to ponad 385 mln złotych, dofinansowanie w wysokości prwie 267 mln złotych będzie pochodziło ze środk&oacute;w Programu Operacyjnego Rozw&oacute;j Polski Wschodniej 2007-2013. Planowany termin zakończenia budowy sieci to 31 lipca 2015 r.</p>\r\n<p>\r\n	Projekt jest realizowany w Departamencie Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie.</p>\r\n<p>\r\n	KORZYŚCI DLA REGIONU:</p>\r\n<p>\r\n	1. Poprawa poziomu życia mieszkańc&oacute;w i aktywizacja społeczności lokalnych:</p>\r\n<p>\r\n	- usprawnienie i obniżenie koszt&oacute;w elektronicznej komunikacji,<br />\r\n	- przeciwdziałanie wykluczeniu cyfrowemu,<br />\r\n	- zwiększenie poziomu wiedzy i kompetencji mieszkańc&oacute;w,<br />\r\n	- ułatwienie mieszkańcom regionu załatwiania spraw administracyjnych, i innych formalności oraz zaspokojenie potrzeb informacyjnych, analitycznych i edukacyjnych za pomocą technologii informatycznych (budowa społeczeństwa informacyjnego / cyfrowego),<br />\r\n	- wzrost liczby nowych inwestycji z zakresu IT w regionie.</p>\r\n<p>\r\n	2. Aktywizacja zawodowa i proces zmian na rynku pracy:</p>\r\n<p>\r\n	- uelastycznienie rynku pracy,<br />\r\n	- wzrost poziomu og&oacute;lnego wykształcenia ludności dający podstawę do budowy gospodarki opartej na wiedzy,<br />\r\n	- rozw&oacute;j kształcenia ustawicznego dający szansę na wzrost poziomu kwalifikacji<br />\r\n	i umiejętności zasob&oacute;w ludzkich w regionie,<br />\r\n	- powstawanie nowych dziedzin działalności gospodarczej, opartych na nowoczesnych technologiach informatycznych i telekomunikacyjnych,<br />\r\n	- rozw&oacute;j inwestycji i usług w obszarze &bdquo;ostatniej mili&rdquo;,<br />\r\n	- ułatwienie aktywności os&oacute;b niepełnosprawnych oraz wzrost perspektyw w ich dostępie do pracy (m.in. poprzez wzrost możliwości świadczenia telepracy).</p>\r\n<p>\r\n	3. Przyśpieszenie proces&oacute;w gospodarczych:</p>\r\n<p>\r\n	- poprawa wizerunku regionu,<br />\r\n	- likwidacja barier technologicznych, wzrost możliwości wdrażania najnowszych rozwiązań technologicznych,<br />\r\n	- oddolne otwarcie na nowe rynki zbytu i usługi,<br />\r\n	- rozw&oacute;j kontakt&oacute;w międzynarodowych firm z regionu i wzrost ich świadomości w zakresie konkurowania w oparciu o wiedzę i nowe technologie.</p>\r\n<p>\r\n	4. Proces zmian na rynku usług teleinformatycznych:</p>\r\n<p>\r\n	- spadek cen usług dostępu do Internetu,<br />\r\n	- demonopolizacja rynku usług komunikacyjnych.</p>\r\n<p>\r\n	5. Podniesienie poziomu edukacji.</p>\r\n<p>\r\n	6. Zr&oacute;wnanie szans w dostępie do informacji.</p>\r\n<p>\r\n	7. Usprawnienie jakości pracy samorząd&oacute;w lokalnych.</p>\r\n<p>\r\n	8. Rozw&oacute;j infrastruktury i usług społeczeństwa informacyjnego:</p>\r\n<p>\r\n	e-nauka<br />\r\n	e-praca<br />\r\n	e-zdrowie<br />\r\n	e-biznes<br />\r\n	e-handel<br />\r\n	e-urząd</p>', 'porada-image-54086261012dc.jpeg', 0, '2908 km sieci szerokopasmowej na terenie województwa lubelskiego', '2908, sieci, szerokopasmowej, terenie, województwa, lubelskiego, maja, 2013', '22 maja 2013 r.', '2014-09-04 15:00:16', NULL, '2013-05-22 00:00:00'),
(37, NULL, 2, NULL, 0, 'Badanie fokusowe dot. Kluczowych Technologii Wspomagających', 'badanie-fokusowe-dot-kluczowych-technologii-wspomagajacych', '<p>\r\n	W dniu 5 kwietnia 2013 r. w siedzibie Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się spotkanie fokusowe będące jednym z element&oacute;w realizowanego w ramach projektu RSZZG badania dziedzinowego pt.: &bdquo;Ocena innowacyjności i konkurencyjności regionalnej gospodarki z punktu widzenia stosowania i rozwoju Kluczowych Technologii Wspomagających&quot;.</p>', '<p>\r\n	W dniu 5 kwietnia 2013 r. w siedzibie Departamentu Gospodarki i Innowacji Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubelskiego w Lublinie odbyło się spotkanie fokusowe będące jednym z element&oacute;w realizowanego w ramach projektu RSZZG badania dziedzinowego pt.: &bdquo;Ocena innowacyjności i konkurencyjności regionalnej gospodarki z punktu widzenia stosowania i rozwoju Kluczowych Technologii Wspomagających&quot;.</p>\r\n<p>\r\n	Celem badania jest uzyskanie pogłębionej wiedzy nt. stopnia zaangażowania jednostek naukowych Lubelszczyzny w proces tworzenia i rozwoju technologii KET, a także poziomu wykorzystywania tych technologii przez przemysł i gospodarkę regionu.</p>\r\n<p>\r\n	Miałem przyjemność otworzyć i moderować spotkanie.</p>', 'porada-image-5408631595696.jpeg', 0, 'Badanie fokusowe dot. Kluczowych Technologii Wspomagających', 'Badanie, fokusowe, dot., Kluczowych, Technologii, Wspomagających, dniu, kwietnia, 2013', 'W dniu 5 kwietnia 2013 r.', '2014-09-04 15:03:17', NULL, '2013-04-05 00:00:00'),
(38, NULL, 10, NULL, 0, 'TEST 1', 'test-1', '<p>\r\n	TEST 1</p>', NULL, 'porada-image-5408d74852ea3.jpeg', 0, 'TEST 1', 'TEST', '', '2014-09-04 23:19:03', NULL, '2014-09-04 00:00:00'),
(39, NULL, 11, NULL, 0, 'TEST 2', 'test-2', '<p>\r\n	TEST 2</p>', NULL, 'porada-image-5408d77427a88.jpeg', 0, 'TEST 2', 'TEST', '', '2014-09-04 23:19:47', NULL, '2014-09-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `porada_category`
--

CREATE TABLE `porada_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL,
  `color` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `menu_bg_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `porada_category`
--

INSERT INTO `porada_category` (`id`, `name`, `slug`, `arrangement`, `color`, `photo`, `description`, `menu_bg_img`) VALUES
(1, 'Energetyka', 'energetyka', 3, 'ff7a0c', 'category-image-53fe119f595c2.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu wykorzystania potencjału regionalnych zasob&oacute;w energetycznych i rozwoju alternatywnych źr&oacute;deł energii.</p>', 'category-thumb-53fe5f0fa501f.jpeg'),
(2, 'Cyfryzacja', 'cyfryzacja', 1, '1188c9', 'category-image-5401a8f0dc35a.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu zwiększenia dostępności technologii informacyjno-komunikacyjnych i rozwoju społeczeństwa cyfrowego.</p>', 'category-thumb-53fe5c76c51e4.jpeg'),
(3, 'Inwestycje', 'inwestycje', 4, '69b3ca', 'category-image-5404a2d450d79.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu i projekt&oacute;w prorozwojowych oraz zwiększania atrakcyjności inwestycyjnej.</p>', 'category-thumb-53fe5f24020fc.jpeg'),
(4, 'Nauka', 'nauka', 5, '7cbb38', 'category-image-53fe30e5e9ef5.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu inwestowania w edukację, naukę, kapitał ludzki oraz ustawiczne kształcenie.</p>', 'category-thumb-53fe5f2c4d22c.jpeg'),
(5, 'Planowanie strategiczne', 'planowanie-strategiczne', 2, 'ffb324', 'category-image-53fe26031fc71.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu trafnego określania cel&oacute;w rozwojowych i skutecznego zarządzania procesami rozwojowymi w gospodarce.</p>', 'category-thumb-53fe5f05a7589.jpeg'),
(6, 'Przedsiębiorczość', 'przedsiebiorczosc', 6, 'ee4627', 'category-image-53fe31971f949.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu podnoszenia konkurencyjności i innowacyjności firm, pozyskiwania inwestycji zagranicznych oraz tworzenia nowych miejsc pracy.</p>', 'category-thumb-53fe5f34d8a81.jpeg'),
(7, 'Innowacje', 'innowacje', 7, 'a82a5f', 'category-image-53fe3226e015d.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu wzmacniania rozwoju sektora badawczo-rozwojowego oraz wykorzystania wynik&oacute;w prac badawczych w gospodarce.</p>', 'category-thumb-53fe5f3d3d7d2.jpeg'),
(10, 'Współpraca i partnerstwa gospodarcze', 'wspolpraca-i-partnerstwa-gospodarcze', 8, '831b7a', 'category-image-5409e9faba877.jpeg', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu budowy sieci wsp&oacute;łpracy branżowej i klastrowej oraz partnerstw gospodarczych.</p>', 'category-thumb-54085956b0b0c.jpeg'),
(11, 'Strategia spójności', 'strategia-spojnosci', 9, 'd1b5aa', 'category-image-5404c7d1d1abe.jpeg', '<p>\r\n	testowy opis</p>', 'category-thumb-5408596656881.jpeg');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pricing_item`
--

CREATE TABLE `pricing_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `promo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` decimal(6,2) DEFAULT NULL,
  `promo_price` decimal(6,2) DEFAULT NULL,
  `karnet` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `pricing_item`
--

INSERT INTO `pricing_item` (`id`, `name`, `description`, `promo_description`, `price`, `promo_price`, `karnet`, `arrangement`) VALUES
(1, 'Open', 'nieograniczony wstęp do centrum przez okres 31 dni', 'Promocja obowiązuje tylko w czerwcu', 99.00, 75.00, 1, 1),
(2, 'Bezpieczne Solarium - Słoneczna Łączka', '1 minuta', NULL, 1.00, NULL, 0, 10),
(3, 'Sauna (1-2 osoby)', '1 godzina', NULL, 25.00, NULL, 0, 7),
(4, 'Sauna (3-4 osoby)', '1 godzina', NULL, 35.00, NULL, 0, 8),
(5, 'Kompleksowe Odchudzanie', 'Karnet Open, dwie konsultacje motywacyjno dietetyczne, analiza składu ciała', NULL, 199.00, NULL, 1, 3),
(7, 'Morning Fit', 'wstęp na siłownię przez okres 31 dni w godzinach porannych (codziennie do godziny czternastej)', NULL, 70.00, NULL, 1, 2),
(8, 'Dwie konsultacje dietetyczno-motywacyjne', 'Plan żywieniowy na miesiąc + analiza składu ciała', NULL, 120.00, NULL, 0, 9),
(9, 'Jednorazowe wejście na siłownie', NULL, NULL, 15.00, NULL, 0, 12),
(10, 'Open na okres 3 miesiące', 'nieograniczony wstęp do centrum przez okres 93 dni', NULL, 240.00, NULL, 0, 4),
(11, 'Karnet 10 wejść', NULL, NULL, 99.00, NULL, 0, 6),
(12, 'Karnet 5 wejść', NULL, NULL, 60.00, NULL, 0, 5),
(13, 'Analiza składu Ciała wraz z omówieniem', NULL, NULL, 20.00, NULL, 0, 11),
(14, 'Pakiet Antycelulitowy', '(dla osób posiadających karnet wstępu cena: 135 zł)', NULL, 165.00, NULL, 0, 13),
(15, 'Karnet Vibro', NULL, NULL, 50.00, NULL, 0, 14),
(16, 'Jednorazowe Wejście Vibro', NULL, NULL, 8.00, NULL, 0, 15),
(18, 'Karnet solarium 15 sesji', '300 min', NULL, 220.00, 110.00, 0, 16),
(19, 'Karnet solarium 10 sesji', '200 minut', NULL, 160.00, 80.00, 0, 17),
(20, 'Karnet solarium 5 sesji', '100 min', NULL, 85.00, 42.50, 0, 18),
(21, 'Karnet solarium 3 sesje', '60 min', NULL, 50.00, 25.00, 0, 19);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `photo` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `active` int(11) NOT NULL,
  `scheme` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `project`
--

INSERT INTO `project` (`id`, `category_id`, `title`, `content`, `photo`, `start_date`, `end_date`, `active`, `scheme`) VALUES
(2, 5, 'Testowy projekt', '<p>\r\n	Testowy opis projektu x</p>', 'porada-image-53f3c2076aa14.jpeg', '2009-03-04 00:00:00', '2011-06-04 00:00:00', 0, '333333'),
(3, 6, 'Tworzenie i rozwój sieci współpracy centrów obsługi inwestora.', '<p>\r\n	<span style="font-family: Arial; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">W dniu 29 grudnia 2009 r. PAIiIZ S.A. podpisała z PARP Umowę o dofinansowanie Projektu nr POPW.01.04.02-00-002/09 &bdquo;Tworzenie i rozw&oacute;j sieci wsp&oacute;łpracy centr&oacute;w obsługi inwestora&quot;.&nbsp;</span></p>\r\n<p>\r\n	<span style="font-family: Arial; font-size: 14px; text-align: justify; background-color: rgb(255, 255, 255);">Przedmiotem powyższego Projektu jest stworzenie sieci centr&oacute;w obsługi inwestor&oacute;w w regionie Polski Wschodniej, kt&oacute;rej wiodącym celem będzie wspieranie procesu doskonalenia poziomu potencjału gospodarczego oraz atrakcyjniści inwestycyjnej region&oacute;w Polski Wschodniej.&nbsp;</span></p>\r\n<p>\r\n	<font face="Arial"><span style="font-size: 14px; background-color: rgb(255, 255, 255);">Projekt jest skierowany m.in. do: przedsiębiorc&oacute;w zagranicznych i krajowych, zainteresowanych ulokowaniem nakład&oacute;w finansowych na terenach Polski Wschodniej; zagranicznych i krajowych izb gospodarczych, organizacji i stowarzyszeń gospodarczych jak r&oacute;wnież podmiot&oacute;w okołobiznesowych, jednostek samorządu terytorialnego szczebla regionalnego, powiatowego i lokalnego.&nbsp;</span></font></p>\r\n<p>\r\n	<span style="font-family: Arial; font-size: 14px; background-color: rgb(255, 255, 255);">Projekt przewiduje zastosowanie szeregu działań systemowych mających na celu stworzenie i rozw&oacute;j sieci centr&oacute;w obsługi inwestora w makroregionie działających w oparciu o jednolite standardy. Wsparcie przewidziane jest m.in. na takie działania jak zakup środk&oacute;w trwałych, leasing samochod&oacute;w, organizację krajowych i zagranicznych seminari&oacute;w / warsztat&oacute;w dla rozwoju kadr obsługujących inwestor&oacute;w, wykonanie i wdrożenie ekspertyz dotyczących oferty inwestycyjnej makroregionu, budowa i utrzymanie aktualnej bazy danych z ofertą centr&oacute;w obsługi inwestora.&nbsp;</span><span style="font-family: Arial; font-size: 14px; background-color: rgb(255, 255, 255);">Partnerzy Projektu utworzą jedną, ponadregionalną sieć wsp&oacute;łpracujących ze sobą instytucji działających regionalnie na rzecz wspierania i&nbsp;podnoszenia atrakcyjności inwestycyjnej poszczeg&oacute;lnych region&oacute;w Polski Wschodniej.</span></p>', 'porada-image-540717bb9b7b9.png', '2010-01-01 00:00:00', '2015-06-30 00:00:00', 0, '#808080'),
(4, 6, 'Wsparcie dla sieci Centrów Obsługi Inwestorów i Eksporterów.', '<p>\r\n	Ponieważ decyzja o rozpoczęciu działalności w skali międzynarodowej wymaga nie tylko wysiłku organizacyjnego i finansowego ale także zdobycia niezbędnej wiedzy dotyczącej rynk&oacute;w zagranicznych czy regulacji regulacji obowiązujących w działalności eksportowej. Jednak w przypadku mniejszych czy też mniej doświadczonych firm stanowi to dość duże wyzwanie a nawet barierę wyjścia poza rynek krajowy.</p>\r\n<p>\r\n	W związku z tym został uruchomiony szereg instrument&oacute;w nających na celu wsparcie umiędzynarodowienia działalności gospodarczej. Jednym z nich jest System Centr&oacute;w Obsługi Inwestor&oacute;w i Ekspert&oacute;w (COIE), kt&oacute;ry został utworzony w ramach Poddziałania 6.2.1 Programu Operacyjnego Innowacyjna Gospodarka (2007-2013). Elementem tego systemu jest &nbsp;utworzone lubelskie COIE funkcjonujące w strukturach Urzędu Marszałkowskiego Wojew&oacute;dztwa Lubleskiego.&nbsp;</p>', 'porada-image-54071f3297f30.jpeg', '2009-01-01 00:00:00', '2013-12-01 00:00:00', 0, '#808080');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_short` longtext COLLATE utf8_unicode_ci,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `services`
--

INSERT INTO `services` (`id`, `gallery_id`, `category_id`, `title`, `slug`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`, `price`, `time`) VALUES
(2, NULL, 11, 'Dokument lorem ipsum x', 'dokument-lorem-ipsum-x', '<p>\r\n	12</p>', '<p>\r\n	12</p>', 'services-image-543ee9b626769.pdf', 1, 'to change', 'to change', 'to change', '2014-10-15 23:40:05', NULL, '2014-10-15 00:00:00', '12', '12');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `services_category`
--

CREATE TABLE `services_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `photo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `arrangement` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `services_category`
--

INSERT INTO `services_category` (`id`, `title`, `photo`, `slug`, `arrangement`) VALUES
(11, 'test', 'category-image-543db28ef3387.jpeg', 'test', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_title', 'Treść domyślnego tagu Title', 'Sokołów Małopolski'),
(2, 'seo_description', 'Treść domyślnego metatagu Description', 'Sokołów Małopolski'),
(3, 'seo_keywords', 'Treść domyślnego metatagu Keywords', ''),
(4, 'limit_news', 'Ilość aktualności wyświetlanych na jednej stronie listowania', '5'),
(19, 'email_to_kontakt', 'Adres e-mail na który mają być wysyłane wiadomości z formularza kontaktowego', 'lpiwowar@gmail.com'),
(21, 'herb_text', 'Kontakt - text obok herbu', 'Sokołów Małopolski'),
(22, 'herb_desc', 'Kontakt - opis pod herbem', 'Urząd Gminy i Miasta w Sokołowie Małopolskim'),
(29, 'added_texts_address', 'Kontakt - adres', '36-050 Sokołów Młp.<br/>\r\n                                ul. Rynek 1'),
(30, 'kontakt_konto', 'Kontakt - konto', '07 9182 0006 0000 0390 2000 0020'),
(31, 'kontakt_bank', 'Kontakt - bank', 'Bank Spółdzielczy Sokołów Młp.'),
(32, 'kontakt_gmina_nip', 'Kontakt - gmina Nip', '517-01-21-981'),
(33, 'kontakt_gmina_regon', 'Kontakt - gmina Regon', '690 582 134'),
(34, 'kontakt_urzad_nip', 'Kontakt - urząd NIP', '814-12-58-727'),
(35, 'kontakt_urzad_regon', 'Kontakt - urząd REGON', '000 529 634'),
(36, 'kontakt_phone', 'Kontakt - telefon', '(017) 77 29 019'),
(37, 'kontakt_fax', 'Kontakt - fax', '(017) 77 29 019 wew. 28'),
(38, 'kontakt_mail', 'Kontakt - email', 'ugim@sokolow-mlp.pl'),
(39, 'bip_url', 'adres url BIP', '#'),
(40, 'tree_url', 'Ikona drzewa url', '#'),
(41, 'wirtualny_url', 'Url wirtualnego urzedu', '/wirtualny-urzad');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo_width` int(11) NOT NULL,
  `photo_height` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider`
--

INSERT INTO `slider` (`id`, `label`, `photo_width`, `photo_height`) VALUES
(1, 'Slider-SG', 980, 432);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `link_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `routeParameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider_photo`
--

INSERT INTO `slider_photo` (`id`, `slider_id`, `photo`, `title`, `content`, `link_name`, `type`, `route`, `routeParameters`, `url`, `onclick`, `arrangement`) VALUES
(5, 1, 'slider-image-54088fce3c3f0.jpeg', 'Przedsiębiorczość / 2009', '<p>\r\n	Podnoszenie konkurencyjności i innowacyjności firm, pozyskiwanie inwestycji zagranicznych, tworzenie nowych miejsc pracy.</p>', 'Przedsiębiorczość', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/przedsiebiorczosc', NULL, 2),
(6, 1, 'slider-image-54088fdd9d9d3.jpeg', 'Nauka / 2010', '<p>\r\n	Inwestowanie w edukację, naukę, kapitał ludzki oraz ustawiczne kształcenie.</p>', 'Nauka', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/nauka', NULL, 3),
(7, 1, 'slider-image-54088ffe9ad88.jpeg', 'Inwestycje / 2011', '<p>\r\n	Realizacja inwestycji i projekt&oacute;w prorozwojowych oraz zwiększanie atrakcyjności inwestycyjnej.</p>', 'Inwestycje', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/inwestycje', NULL, 4),
(8, 1, 'slider-image-54089009a0e30.jpeg', 'Energetyka / 2012', '<p>\r\n	Wykorzystanie potencjału regionalnych zasob&oacute;w energetycznych i rozw&oacute;j alternatywnych źr&oacute;deł energii.</p>', 'Energetyka', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/energetyka', NULL, 5),
(9, 1, 'slider-image-54089014bf9bc.jpeg', 'Planowanie strategiczne / 2013', '<p>\r\n	Trafne określanie cel&oacute;w rozwojowych i skuteczne zarządzanie procesami rozwojowymi w gospodarce.</p>', 'Planowanie strategiczne', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/planowanie-strategiczne', NULL, 6),
(10, 1, 'slider-image-5408902431bcd.jpeg', 'Cyfryzacja / 2014', '<p>\r\n	Zwiększanie dostępności technologii informacyjno-komunikacyjnych i rozw&oacute;j społeczeństwa cyfrowego.</p>', 'Cyfryzacja', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/cyfryzacja', NULL, 7),
(11, 1, 'slider-image-54088f961a409.jpeg', 'Innowacje / 2008', '<p>\r\n	Wzmacnianie sektora badawczo-rozwojowego oraz wykorzystywanie wynik&oacute;w prac badawczych w gospodarce.&nbsp;</p>', 'Innowacje', 'url', 'lscms_dietetyk', NULL, '/moja-dzialalnosc/kategoria/innowacje', NULL, 1),
(14, 1, 'slider-image-540890a90bb43.jpeg', 'Sieci współpracy i partnerstwa gospodarcze', '<p>\r\n	Działania, kt&oacute;re podejmowałem w celu budowy sieci wsp&oacute;łpracy branżowej i klastrowej oraz partnerstw gospodarczych</p>', 'Sieci współpracy i partnerstwa gospodarcze', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/wspolpraca-i-partnerstwa-gospodarcze', NULL, 8),
(15, 1, 'slider-image-540890b6f2108.jpeg', 'Strategia spójności', '<p>\r\n	Opis testowy</p>', 'Strategia spójności', 'url', NULL, NULL, '/moja-dzialalnosc/kategoria/strategia-spojnosci', NULL, 9);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Indexes for table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Indexes for table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Indexes for table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Indexes for table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Indexes for table `fos_group`
--
ALTER TABLE `fos_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_4B019DDB5E237E06` (`name`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `IDX_B3C77447A76ED395` (`user_id`),
  ADD KEY `IDX_B3C77447FE54D947` (`group_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_77D360B841F8D70E` (`karnet_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person_wizyta`
--
ALTER TABLE `person_wizyta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8126474B217BBB47` (`person_id`);

--
-- Indexes for table `porada`
--
ALTER TABLE `porada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_12857A934E7AF8F` (`gallery_id`),
  ADD KEY `IDX_12857A9312469DE2` (`category_id`);

--
-- Indexes for table `porada_category`
--
ALTER TABLE `porada_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7234C9C75E237E06` (`name`);

--
-- Indexes for table `pricing_item`
--
ALTER TABLE `pricing_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_12857A934E7AF8F` (`gallery_id`),
  ADD KEY `IDX_12857A9312469DE2` (`category_id`);

--
-- Indexes for table `services_category`
--
ALTER TABLE `services_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC71007EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9203C87C2CCC9638` (`slider_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `fos_group`
--
ALTER TABLE `fos_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT dla tabeli `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT dla tabeli `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `person_wizyta`
--
ALTER TABLE `person_wizyta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `porada`
--
ALTER TABLE `porada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT dla tabeli `porada_category`
--
ALTER TABLE `porada_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `pricing_item`
--
ALTER TABLE `pricing_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT dla tabeli `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `services_category`
--
ALTER TABLE `services_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT dla tabeli `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Ograniczenia dla tabeli `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_group` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `karnet_zamowienie`
--
ALTER TABLE `karnet_zamowienie`
  ADD CONSTRAINT `FK_77D360B841F8D70E` FOREIGN KEY (`karnet_id`) REFERENCES `pricing_item` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `person_wizyta`
--
ALTER TABLE `person_wizyta`
  ADD CONSTRAINT `FK_8126474B217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `porada`
--
ALTER TABLE `porada`
  ADD CONSTRAINT `FK_12857A9312469DE2` FOREIGN KEY (`category_id`) REFERENCES `porada_category` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `FK_12857A934E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD CONSTRAINT `FK_9203C87C2CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

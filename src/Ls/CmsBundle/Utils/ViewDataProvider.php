<?php
/**
 * Created by PhpStorm.
 * User: Lukasz
 * Date: 17.10.14
 * Time: 00:05
 */

namespace Ls\CmsBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerAware;
use Ls\CmsBundle\Entity\MenuItem;
use Knp\Menu\FactoryInterface;


class ViewDataProvider extends ContainerAware{


    public function __construct($em,$sc)
    {
        $this->em = $em;
        $this->container = $sc;
    }

    public function slider(){

        $em = $this->container->get('doctrine')->getManager();

        $partners = $em->createQueryBuilder()
            ->select('m')
            ->from('LsCmsBundle:Partners', 'm')
            //->where('m.location = :location')
            /*->andWhere('m.parent is NULL')*/
           /* ->orderBy('m.arrangement', 'ASC')*/
           // ->setParameter('location', $options['location'])
            ->getQuery()
            ->getResult();

        return $partners;

    }





    public function forecast(){

        $dayNames = array(
            0 => 'Nd.',
            1 => 'Pn.',
            2 => 'Wt.',
            3 => 'Śr.',
            4 => 'Czw.',
            5 => 'Pt.',
            6 => 'Sob.'
        );

        if(!file_exists('forecast_data.json')){
            $data = file_get_contents('http://api.openweathermap.org/data/2.5/forecast/daily?lat=50.2306326&lon=22.1175726&cnt=5&mode=json&units=metric');
            file_put_contents('forecast_data.json',$data);
            file_put_contents('last_forecast_update.txt',time());
        }

        $lastUpdate = file_get_contents('last_forecast_update.txt');
        $lastUpdateDiff = time() - intval($lastUpdate);
        if($lastUpdateDiff > 3600){
            $data = file_get_contents('http://api.openweathermap.org/data/2.5/forecast/daily?lat=50.2306326&lon=22.1175726&cnt=5&mode=json&units=metric');

            if(strlen($data) > 5){
                file_put_contents('forecast_data.json',$data);
                file_put_contents('last_forecast_update.txt',time());
            }

        }

        $data =  file_get_contents('forecast_data.json');
        $obj = json_decode($data);

        foreach($obj->list as $item){
            /*var_dump($item);*/
            // $item->weather[0]->icon

            $item->dayname = $dayNames[date('w',$item->dt)];

            $icon = $item->weather[0]->icon;
            if(!file_exists('forecast/'.$icon.'.png')){
                file_put_contents('forecast/'.$icon.'.png',file_get_contents('http://openweathermap.org/img/w/'.$icon.'.png'));
            }

        }



        return $obj;

    }




}
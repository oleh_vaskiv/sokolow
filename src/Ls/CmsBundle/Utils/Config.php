<?php

namespace Ls\CmsBundle\Utils;

use Ls\CmsBundle\Entity\Setting;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Config {

    protected $em;
    protected $repo;
    protected $container;
    protected $settings = array();
    protected $is_loaded = false;

    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
    }

    public function get($name, $default) {
        if (array_key_exists($name, $this->settings)) {
            return $this->settings[$name];
        }
        return $default;
    }

    public function all() {
        $settings = array();

        if ($this->is_loaded) {
            return $this->settings;
        }

        foreach ($this->getRepo()->findAll() as $setting) {
            $settings[$setting->getLabel()] = $setting->getValue();
        }

        $this->settings = $settings;
        $this->is_loaded;

        return $settings;
    }

    protected function getRepo() {
        if ($this->repo === null) {
            $this->repo = $this->em->getRepository(get_class(new Setting()));
        }

        return $this->repo;
    }
}

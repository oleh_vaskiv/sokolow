<?php

// src/Ls/CmsBundle/Utils/Tools.php

namespace Ls\CmsBundle\Utils;

use Symfony\Component\Form\Form;

class Tools
{

    static public function truncateWord($string, $limit, $delimiter)
    {
        $new = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string . ' ', 0, $limit));

        if (strlen($string) > $limit) {
            $new .= $delimiter;
        }

        return $new;
    }

    static public function thumbName($filename, $appendix)
    {
        $temp = explode('.', $filename);
        $ext = end($temp);
        $thumbname = substr($filename, 0, strlen($filename) - (strlen($ext) + 1)) . $appendix . "." . $ext;
        return $thumbname;
    }

    static public function dump($value)
    {
        echo '<pre style="color: red;">';
        print_r($value);
        echo '</pre>';
    }

    static function changeSecToTime($time)
    {
        $hour = floor($time / 3600);
        $min = floor(($time - ($hour * 3600)) / 60);
        $sec = $time - ($hour * 3600) - ($min * 60);
        if ($min == 0)
            $min = '00';

        if ($min < 10 && $min > 0)
            $min = implode('', array('0', $min));

        if ($sec == 0)
            $sec = '00';

        if ($sec < 10 && $sec > 0)
            $sec = implode('', array('0', $sec));

        return $hour . ':' . $min . ':' . $sec;
    }

    static public function getFormErrors(Form $form)
    {
        $errors = $form->getErrors();
        foreach ($form->all() as $child) {
            foreach ($child->getErrors() as $key => $error) {
                $template = $error->getMessageTemplate();
                $parameters = $error->getMessageParameters();

                foreach ($parameters as $var => $value) {
                    $template = str_replace($var, $value, $template);
                }

                $errors[$child->getName()][] = $template;
            }
        }
        return $errors;
    }
}

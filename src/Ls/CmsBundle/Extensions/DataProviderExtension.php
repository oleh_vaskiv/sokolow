<?php

namespace Ls\CmsBundle\Extensions;

use Ls\CmsBundle\Utils\ViewDataProvider;

class DataProviderExtension extends \Twig_Extension {

    protected $data_provider;

    function __construct(ViewDataProvider $obj) {
        $this->data_provider = $obj;
    }

    public function getGlobals() {
        return array(
           /* 'settings' => $this->data_provider->all(),*/
            'data_provider' => $this->data_provider
        );
    }

    public function getName() {
        return 'data_provider';
    }

}
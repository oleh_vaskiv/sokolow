<?php

namespace Ls\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonWizytaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, array('label' => 'Imię...', 'required' => true,))
            ->add('lastname', null, array('label' => 'Nazwisko...', 'required' => true,))
            ->add('phone', null, array('label' => 'Numer kontaktowy...', 'required' => false,))
            ->add('email', null, array('label' => 'E-mail zwrotny...', 'required' => true,))
            ->add('termin', 'date', array('label' => 'Termin...', 'required' => true, 'widget' => 'single_text', 'format' => 'dd-MM-yyyy'))
            ->add('content', 'textarea', array('label' => 'Treść wiadomości...', 'required' => true,))
            ->add('personid', 'hidden', array('mapped' => false))
            ->add('submit', 'submit', array('label' => 'Umów się'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\CmsBundle\Entity\PersonWizyta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wizyta_form';
    }
}

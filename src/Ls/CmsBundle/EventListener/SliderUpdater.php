<?php

namespace LS\CMSBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CmsBundle\Entity\SliderPhoto;

class SliderUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (null === $entity->getArrangement()) {
                $slider = $entity->getSlider();
                $query = $em->createQueryBuilder()
                        ->select('COUNT(c.id)')
                        ->from('LsCmsBundle:SliderPhoto', 'c')
                        ->where('c.slider = :slider')
                        ->setParameter('slider', $slider)
                        ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (null === $entity->getArrangement()) {
                $slider = $entity->getSlider();
                $query = $em->createQueryBuilder()
                        ->select('COUNT(c.id)')
                        ->from('LsCmsBundle:SliderPhoto', 'c')
                        ->where('c.slider = :slider')
                        ->setParameter('slider', $slider)
                        ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof SliderPhoto) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $slider = $entity->getSlider();

                $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsCmsBundle:SliderPhoto', 'c')
                        ->where('c.arrangement > :arrangement')
                        ->andWhere('c.slider = :slider')
                        ->setParameters(
                                array(
                                    'arrangement' => $arrangement,
                                    'slider' => $slider
                                )
                        )
                        ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['type'] = 'SliderPhoto';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto();
        }
    }

}
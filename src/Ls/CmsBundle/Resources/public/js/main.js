$(document).ready(function() {
    $("a.fancybox").fancybox({
        'titlePosition': 'outside',
        'overlayColor': '#000',
        'overlayOpacity': 0.9
    });

    $('.popup_close').live('click', function() {
        $('div[id^="qtip-"]').each(function() { //search for remaining objects
            _qtip2 = $(this).data("qtip"); //access the data where destroy() exist.
            //if it's a proper qtip2 object then call the destroy method.
            if (_qtip2 != undefined) {
                // the "true" is for immediate destroy
                _qtip2.destroy(true);
            }
            //if everything went right the data and the remaining objects in the body must be gone.
        });
        $(this).parent().find('.popup_body').html('');
        $(this).parent().parent().stop(true, true).fadeOut(250);
    });
});

$(document).ajaxStart(function() {
    $('.preloader_container').show();
});

$(document).ajaxStop(function() {
    $('.preloader_container').hide();
});

function showWizyta(url, id) {
    $.ajax({
        type: "post",
        url: url,
        data: {
            'id': id
        },
        success: function(response) {
            $('#popup_wizyta').find('.popup_content').find('.popup_body').html(response);
            $('html, body').animate({scrollTop: 128}, 750);
            $('#popup_wizyta').stop(true, true).fadeIn(250);
            $('.datepicker').datepicker($.extend({}, {
                showOn: "both"
            }, $.datepicker.regional["pl"], {
                dateFormat: "dd-mm-yy"
            }));
        }
    });
    return false;
}

function sendWizyta() {
    $('div[id^="qtip-"]').each(function() { //search for remaining objects
        _qtip2 = $(this).data("qtip"); //access the data where destroy() exist.
        //if it's a proper qtip2 object then call the destroy method.
        if (_qtip2 != undefined) {
            // the "true" is for immediate destroy
            _qtip2.destroy(true);
        }
        //if everything went right the data and the remaining objects in the body must be gone.
    });

    var data = $('.wizyta-form').serialize();
    var url = $('.wizyta-form').attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function(response) {
            if (response.result == 'OK') {
                $('.wizyta_form_container').html('');
                var message = document.createElement('div');
                message.setAttribute('class', 'popup_form_success');
                message.textContent = 'Informacja o wizycie została wysłana.';
                $('.wizyta_form_container').append(message);
            } else {
                for (var error in response.errors) {
                    $('.wizyta-' + error).qtip({
                        content: {
                            title: 'Błąd',
                            text: response.errors[error][0]
                        },
                        position: {
                            my: 'center left',
                            at: 'right center'
                        },
                        show: {
                            ready: true,
                            event: false
                        },
                        hide: {
                            event: 'click focus'
                        },
                        style: {
                            classes: 'qtip-green qtip-rounded qtip-shadow'
                        }
                    });
                }
            }
        }
    });

    return false;
}

function showKarnet(url) {
    $.ajax({
        type: "post",
        url: url,
        success: function(response) {
            $('#popup_karnet').find('.popup_content').attr('class', 'popup_content popup_karnet_step_1_content');
            $('#popup_karnet').find('.popup_content').find('.popup_body').html(response);
            $('html, body').animate({scrollTop: 128}, 750);
            $('#popup_karnet').stop(true, true).fadeIn(250);
            $('.karnet_item_first').find('.karnet_item_checbox').addClass('karnet_item_checbox_selected');
            var id = $('.karnet_item_first').find('.karnet_item_checbox').attr('id').replace('karnet-id-', '');
            $('#karnet-id').val(id);
            $('.karnet_item_checbox').live('click', function() {
                $('.karnet_item_checbox').removeClass('karnet_item_checbox_selected');
                $(this).addClass('karnet_item_checbox_selected');
                var id = $(this).attr('id').replace('karnet-id-', '');
                $('#karnet-id').val(id);
            });
        }
    });

    return false;
}

function submitKarnetStep1() {
    var data = $('#karnet-step-1-form').serialize();
    var url = $('#karnet-step-1-form').attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function(response) {
            $('#popup_karnet').find('.popup_content').attr('class', 'popup_content popup_karnet_step_2_content');
            $('#popup_karnet').find('.popup_content').find('.popup_body').html(response);
        }
    });

    return false;
}

function submitKarnetStep2() {
    var data = $('#karnet-step-2-form').serialize();
    var url = $('#karnet-step-2-form').attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function(response) {
            $('#popup_karnet').find('.popup_content').attr('class', 'popup_content popup_karnet_step_3_content');
            $('#popup_karnet').find('.popup_content').find('.popup_body').html(response);
            initializePseudoSelect();
        }
    });

    return false;
}

function submitKarnetStep3() {
    $('div[id^="qtip-"]').each(function() { //search for remaining objects
        _qtip2 = $(this).data("qtip"); //access the data where destroy() exist.
        //if it's a proper qtip2 object then call the destroy method.
        if (_qtip2 != undefined) {
            // the "true" is for immediate destroy
            _qtip2.destroy(true);
        }
        //if everything went right the data and the remaining objects in the body must be gone.
    });

    var data = $('#karnet-step-3-form').serialize();
    var url = $('#karnet-step-3-form').attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function(response) {
            if (response.result == 'OK') {
                $.ajax({
                    type: "post",
                    url: response.step_4_url,
                    success: function(response) {
                        $('#popup_karnet').find('.popup_content').attr('class', 'popup_content popup_karnet_step_4_content');
                        $('#popup_karnet').find('.popup_content').find('.popup_body').html(response);
                    }
                });
            } else {
                for (var error in response.errors) {
                    $('.karnet-step3-' + error).qtip({
                        content: {
                            title: 'Błąd',
                            text: response.errors[error][0]
                        },
                        position: {
                            my: 'center left',
                            at: 'right center'
                        },
                        show: {
                            ready: true,
                            event: false
                        },
                        hide: {
                            event: 'click focus'
                        },
                        style: {
                            classes: 'qtip-green qtip-rounded qtip-shadow'
                        }
                    });
                }
            }
        }
    });

    return false;
}

function submitKarnetStep4() {
    $('div[id^="qtip-"]').each(function() { //search for remaining objects
        _qtip2 = $(this).data("qtip"); //access the data where destroy() exist.
        //if it's a proper qtip2 object then call the destroy method.
        if (_qtip2 != undefined) {
            // the "true" is for immediate destroy
            _qtip2.destroy(true);
        }
        //if everything went right the data and the remaining objects in the body must be gone.
    });

    var data = $('#karnet-step-4-form').serialize();
    var url = $('#karnet-step-4-form').attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function(response) {
            $('#popup_karnet').find('.popup_content').find('.popup_body').html(response);
        }
    });

    return false;
}

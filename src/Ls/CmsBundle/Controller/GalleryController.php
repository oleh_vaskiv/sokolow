<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Entity\Gallery;
use Ls\CmsBundle\Utils\Tools;

/**
 * Gallery controller.
 *
 */
class GalleryController extends Controller
{

    /**
     * Lists all Gallery entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()->select('a')
            ->from('LsCmsBundle:Gallery', 'a')
            ->getQuery()
            ->getResult();

        $limit = 6;
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities, $page, $limit
        );

        return $this->render('LsCmsBundle:Gallery:index.html.twig', array(
            'entities' => $pagination
        ));
    }

    /**
     * Finds and displays a Gallery entity.
     *
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:Gallery')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }



        $limit = 15;
        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entity->getPhotos(), $page, $limit
        );

        return $this->render('LsCmsBundle:Gallery:show.html.twig', array(
            'entity' => $entity,
            'photos' => $pagination
        ));
    }

}

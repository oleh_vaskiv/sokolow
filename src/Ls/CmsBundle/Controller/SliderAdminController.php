<?php

// src/LS/CMSBundle/Controller/SliderAdminController.php

namespace LS\CMSBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SliderAdminController extends Controller {

    public function photosAction() {
        return new RedirectResponse($this->admin->getChild('ls.cms.admin.sliderphoto')->generateUrl('list'));
    }

}

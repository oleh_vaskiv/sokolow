<?php

// src/Ls/CmsBundle/Controller/MenuItemAdminController.php

namespace Ls\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Sonata\AdminBundle\Controller\CRUDController as Controller;

class MenuItemAdminController extends Controller {

    /**
     * return the Response object associated to the list action
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     *
     * @return Response
     */
    public function listAction() {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $menu_cfg = $this->container->getParameter('ls_cms.menu');
        $locations = $menu_cfg['locations'];
        $menus = array();
        foreach ($locations as $location) {
            $menus[$location['label']]['name'] = $location['name'];
            $menus[$location['label']]['items'] = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsCmsBundle:MenuItem', 'c')
                    ->where('c.location = :location')
                    ->andWhere('c.parent is NULL')
                    ->orderBy('c.arrangement', 'ASC')
                    ->setParameter('location', $location['label'])
                    ->getQuery()
                    ->getResult();
        }

        return $this->render($this->admin->getTemplate('list'), array(
                    'action' => 'list',
                    'menus' => $menus,
        ));
    }

    public function routeParametersAction() {
        $route = $this->getRequest()->request->get('route');
        $search = $this->getRequest()->request->get('search');

        $services = $this->admin->getSearchServices();

        $html = '';

        if (array_key_exists($route, $services)) {
            $service = $services[$route];
            if ($service != null) {
                if ($this->container->has($service)) {
                    $elements = $this->container->get($service)->search($search);

                    if (is_array($elements)) {
                        $html = $this->get('templating')->render('LsCmsBundle:Admin\MenuItem:routeparameters_choice.html.twig', array('elements' => $elements));
                    }
                }
            }
        }

        $response = new Response();
        $response->setContent($html);

        return $response;
    }

    function updateChildren($elements, $parent_id, $arrangement) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsCmsBundle:MenuItem');
        $parent = $repository->find($parent_id);

        $arrangement++;
        foreach ($elements as $element) {
            $item_id = str_replace('list_', '', $element['id']);
            $item = $repository->find($item_id);
            if ($item) {
                $item->setArrangement($arrangement);
                $item->setParent($parent);
                if (isset($element['children'])) {
                    $arrangement = $this->updateChildren($element['children'], $item_id, $arrangement);
                } else {
                    $arrangement++;
                }
                $em->flush();
            }
        }
        
        return $arrangement;
    }

    function saveOrderAction() {
        $elements = $this->getRequest()->request->get('elements');
        $arrangement = 1;

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsCmsBundle:MenuItem');

        if (is_array($elements)) {
            foreach ($elements as $element) {
                $item_id = str_replace('list_', '', $element['id']);
                $item = $repository->find($item_id);
                if ($item) {
                    $item->setArrangement($arrangement);
                    $item->setParent(null);
                    if (isset($element['children'])) {
                        $arrangement = $this->updateChildren($element['children'], $item_id, $arrangement);
                    } else {
                        $arrangement++;
                    }
                    $em->flush();
                }
            }
        }

        $response = new Response();
        $response->setContent('ok');

        return $response;
    }

}

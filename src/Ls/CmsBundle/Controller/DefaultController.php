<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Entity\PersonWizyta;
use Ls\CmsBundle\Entity\KarnetZamowienie;
use Ls\CmsBundle\Form\PersonWizytaType;
use Ls\CmsBundle\Menu\Search;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

class DefaultController extends Controller {

    public function indexAction() {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $news = $qb->select('n')
            ->from('LsCmsBundle:News', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();

        foreach($news as $_news){
            $_news->setContentShort(Tools::truncateWord($_news->getContentShort(), 100, '...'));
        }

        $galleries = $em->createQueryBuilder()->select('a')
            ->from('LsCmsBundle:Gallery', 'a')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();



        return $this->render('LsCmsBundle:Default:index.html.twig', array(
                'news' => $news,
                'galleries' => $galleries
        ));
    }

    public function getCategories(){
    $em = $this->getDoctrine()->getManager();

    $categories = $em->createQueryBuilder()
        ->select('n')
        ->from('LsCmsBundle:PoradaCategory', 'n')
        ->orderBy('n.arrangement', 'ASC')
        ->getQuery()
        ->getResult();



    return $categories;

    }

    public function getCategory($slug){
        $em = $this->getDoctrine()->getManager();

       /* $category = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:PoradaCategory', 'n')
            ->where('n.id = '.$id)
            ->getQuery()
            ->getResult();*/

        $category = $em->getRepository('LsCmsBundle:PoradaCategory')->findOneBySlug($slug);


        return $category;

    }

    public function searchAction(){

            $search = $this->get('request')->query->get('q', '');
             $em = $this->getDoctrine()->getManager();
            $search = mb_strtolower($search);

            $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:News', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

            $ret_arr = array();
            foreach ($items as $item) {
                $ret_arr[] = array(
                    'parameters' => array('slug' => $item->getSlug()),
                    'title' => $item->getTitle(),
                    'text' => $item->getContentShort(),
                    'type' => 'lscms_news_show'
                );
            }


            $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:Page', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();


            foreach ($items as $item) {
                $ret_arr[] = array(
                    'parameters' => array('slug' => $item->getSlug()),
                    'title' => $item->getTitle(),
                    'text' => $item->getContentShort(),
                    'type' => 'lscms_pages_show'
                );
            }


        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsCmsBundle:Gallery', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

            foreach ($items as $item) {
                $ret_arr[] = array(
                    'parameters' => array('slug' => $item->getSlug()),
                    'title' => $item->getTitle(),
                    'text' => $item->getContentShort(),
                    'type' => 'lscms_gallery_show'
                );
            }

        return $this->render('LsCmsBundle:Default:search.html.twig', array(
            'entities' => $ret_arr,
            'query' => $search
        ));

    }


    public function kontaktAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        /*$services = $qb->select(array('n'))
            ->from('LsCmsBundle:ServicesCategory', 'n')
            ->addOrderBy('n.arrangement', 'DESC')
            ->getQuery()
            ->getResult();

        // $services
        $choices = array('' => 'Wybierz usługę');
        foreach($services as $service){
            $choices[(string)$service] = (string)$service;
        }*/



        $form = $this->createFormBuilder()
                ->setAction($this->generateUrl('lscms_kontakt'))
                ->setMethod('POST')
                ->add('name', null, array(
                        'label' => 'Imię i nazwisko',
                        'required' => true,
                        'constraints' => array(
                            new NotBlank(array('message' => 'Wypełnij pole'))
                        )
                    )
                )
                ->add('phone', null, array(
                        'label' => 'Numer telefonu',
                        'required' => true,
                        'constraints' => array(
                            new NotBlank(array('message' => 'Wypełnij pole'))
                        )
                    )
                )
                ->add('email', null, array(
                        'label' => 'E-mail',
                        'required' => true,
                        'constraints' => array(
                            new NotBlank(array('message' => 'Wypełnij pole')),
                            new Email(array('message' => 'Podaj poprawny adres e-mail'))
                        )
                    )
                )
                ->add('content', 'textarea', array(
                        'label' => 'Treść Twojej wiadomości...',
                        'required' => true,
                        'constraints' => array(
                            new NotBlank(array('message' => 'Wypełnij pole'))
                        )
                    )
                )
                ->getForm();

        $form->handleRequest($request);

        $success = false;
        if ($form->isValid()) {

            $email = $form->get('email')->getData();

            $message_txt = '<h3>Wiadomość z formularza kontaktowego</h3>';
            $message_txt .= nl2br($form->get('content')->getData()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię: ' . $form->get('name')->getData() . '<br />';
            $message_txt .= 'E-mail: ' . $form->get('email')->getData() . '<br />';
            $message_txt .= 'Telefon: ' . $form->get('phone')->getData() . '<br />';

            $email_to = $em->getRepository('LsCmsBundle:Setting')->findOneByLabel('email_to_kontakt')->getValue();

            $message = \Swift_Message::newInstance()
                    ->setSubject('Wiadomość z formularza kontaktowego')
                    ->setFrom(array($this->container->getParameter('mailer_user') => 'Strona / Mailer'))
                    ->setTo($email_to)
                    ->setBody($message_txt, 'text/html')
                    ->addPart(strip_tags($message_txt), 'text/plain');

            if (!empty($email)) {
                $message->setReplyTo($email);
            }

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
            $success = true;
        }

        return $this->render('LsCmsBundle:Default:kontakt.html.twig', array(
                    'form' => $form->createView(),
                    'success' => $success

        ));
    }

    public function referencesAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:References', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:references.html.twig', array(
            'entities' => $entities
        ));

    }

    public function partnersAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:Partners', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:partners.html.twig', array(
            'entities' => $entities
        ));

    }

    public function promoAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:Promo', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:promo.html.twig', array(
            'entities' => $entities
        ));

    }

    public function activityAction($slug = null,Request $request) {

        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $category = 0;
        if ($session->has('porada_category')) {
            $category = $session->get('porada_category');
        } else {
            $session->set('porada_category', $category);
        }
        $show = $request->query->get('show',null);

        $categories = $this->getCategories();

        if(!is_null($slug)){

            $categoryEntity = $this->getCategory($slug);

        }else{
            $categoryEntity = false;
        }



        $qb = $em->createQueryBuilder();
        $query = $qb->select('n')
                ->from('LsCmsBundle:Porada', 'n')
                ->where($qb->expr()->isNotNull('n.published_at'))
                ->orderBy('n.published_at', 'DESC');


        if(is_object($categoryEntity)){
            $query->where('n.category = :catId');
            $query->setParameter('catId',$categoryEntity->getId());
        }

        if(!is_null($show)){
            switch($show){
                case 'projekty':
                    $query->andWhere('n.page_type = 1');
                    break;
                case 'wydarzenia':
                    $query->andWhere('n.page_type = 0');
                    break;
                default:
            }
        }


        $entities = $query->getQuery()->getResult();

        // ->where('n.category_id = ?',$catId)
        foreach ($entities as $item) {
            $item->setContentShort(Tools::truncateWord($item->getContentShort(), 250, '...'));
        }

        $page = $this->get('request')->query->get('page', 1);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities, $page, 9
        );

        return $this->render('LsCmsBundle:Default:activity.html.twig', array(
                    'categories' => $categories,
                    'category' => $category,
                    'categoryEntity' => $categoryEntity,
                    'entities' => $pagination,
                    'show' => $show
        ));
    }



}

<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\HttpFoundation\Response;

class PoradaController extends Controller
{

    public function getCategories(){
        $em = $this->getDoctrine()->getManager();

        $categories = $em->createQueryBuilder()
            ->select('n')
            ->from('LsCmsBundle:PoradaCategory', 'n')
            ->orderBy('n.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $categories;
    }

    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsCmsBundle:Porada')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        $qb = $em->createQueryBuilder();
        $eventsInProjects = $qb->select('n')
            ->from('LsCmsBundle:Porada', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->where('n.project = :project_id AND n.id != :currentID')
            ->setParameter('project_id',$entity->getProject())
            ->setParameter('currentID',$entity->getId())
            ->orderBy('n.published_at', 'DESC')
            //->setMaxResults(4)
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Porada:show.html.twig', array(
            'entity' => $entity,
            'projectEntities' => $eventsInProjects,
            'categories' => $this->getCategories()
        ));
    }

    public function ajaxSetCategoryAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $category_id = $request->request->get('category_id');

        $session->set('porada_category', $category_id);
        $session->set('porada_page', 1);

        if($category_id > 0) {
            $category = $em->getRepository('LsCmsBundle:PoradaCategory')->findOneById($category_id);
        }

        $qb = $em->createQueryBuilder();
        $qb->select('a');
        $qb->from('LsCmsBundle:Porada', 'a');
        $qb->where($qb->expr()->isNotNull('a.published_at'));
        if($category_id > 0) {
            $qb->andWhere('a.category = :category');
            $qb->setParameter('category', $category->getId());

        }
        $qb->orderBy('a.published_at', 'DESC');
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, 1, 6
        );
        $pagination->setTemplate('LsCmsBundle::paginator_porady.html.twig');

        $first_column = array();
        $second_column = array();
        $third_column = array();

        foreach($pagination as $key => $entity) {
            if(($key + 3) % 3 == 0) {
                $first_column[] = $entity;
            }
            if(($key + 2) % 3 == 0) {
                $second_column[] = $entity;
            }
            if(($key + 1) % 3 == 0) {
                $third_column[] = $entity;
            }
        }

        return $this->render('LsCmsBundle:Porada:list.html.twig', array(
            'first_column' => $first_column,
            'second_column' => $second_column,
            'third_column' => $third_column,
            'entities' => $pagination,
        ));
    }

    public function ajaxSetPageAction() {
        $em = $this->getDoctrine()->getManager();
        $request = $this->get('request');
        $session = $this->container->get('session');

        $page = $request->request->get('page');

        $session->set('porada_page', $page);
        $category_id = $session->get('porada_category');

        if($category_id > 0) {
            $category = $em->getRepository('LsCmsBundle:PoradaCategory')->findOneById($category_id);
        }

        $qb = $em->createQueryBuilder();
        $qb->select('a');
        $qb->from('LsCmsBundle:Porada', 'a');
        $qb->where($qb->expr()->isNotNull('a.published_at'));
        if($category_id > 0) {
            $qb->andWhere('a.category = :category');
            $qb->setParameter('category', $category->getId());

        }
        $qb->orderBy('a.published_at', 'DESC');
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, $page, 6
        );
        $pagination->setTemplate('LsCmsBundle::paginator_porady.html.twig');

        $first_column = array();
        $second_column = array();
        $third_column = array();

        foreach($pagination as $key => $entity) {
            if(($key + 3) % 3 == 0) {
                $first_column[] = $entity;
            }
            if(($key + 2) % 3 == 0) {
                $second_column[] = $entity;
            }
            if(($key + 1) % 3 == 0) {
                $third_column[] = $entity;
            }
        }

        return $this->render('LsCmsBundle:Porada:list.html.twig', array(
            'first_column' => $first_column,
            'second_column' => $second_column,
            'third_column' => $third_column,
            'entities' => $pagination,
        ));
    }

    public function ajaxGetListAction() {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');

        $page = 1;
        if ($session->has('porada_category')) {
            $page = $session->get('porada_page');
        } else {
            $session->set('porada_page', $page);
        }
        $category_id = $session->get('porada_category');

        if($category_id > 0) {
            $category = $em->getRepository('LsCmsBundle:PoradaCategory')->findOneById($category_id);
        }

        $qb = $em->createQueryBuilder();
        $qb->select('a');
        $qb->from('LsCmsBundle:Porada', 'a');
        $qb->where($qb->expr()->isNotNull('a.published_at'));
        if($category_id > 0) {
            $qb->andWhere('a.category = :category');
            $qb->setParameter('category', $category->getId());

        }
        $qb->orderBy('a.published_at', 'DESC');
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, $page, 6
        );
        $pagination->setTemplate('LsCmsBundle::paginator_porady.html.twig');

        $first_column = array();
        $second_column = array();
        $third_column = array();

        foreach($pagination as $key => $entity) {
            if(($key + 3) % 3 == 0) {
                $first_column[] = $entity;
            }
            if(($key + 2) % 3 == 0) {
                $second_column[] = $entity;
            }
            if(($key + 1) % 3 == 0) {
                $third_column[] = $entity;
            }
        }

        return $this->render('LsCmsBundle:Porada:list.html.twig', array(
            'first_column' => $first_column,
            'second_column' => $second_column,
            'third_column' => $third_column,
            'entities' => $pagination,
        ));
    }

}

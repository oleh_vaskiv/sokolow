<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Entity\PersonWizyta;
use Ls\CmsBundle\Entity\KarnetZamowienie;
use Ls\CmsBundle\Form\PersonWizytaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProjectController extends Controller {



    public function listAction(){

        $em = $this->getDoctrine()->getManager();



        $qb = $em->createQueryBuilder();
        $items = $qb
            ->select('n')
            ->from('LsCmsBundle:Project', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Project:list.html.twig', array(
            'entities' => $items
        ));

    }

    public function detailAction($slug){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsCmsBundle:Project')->findOneBySlug($slug);


        return $this->render('LsCmsBundle:Project:detail.html.twig', array(
            'entity' => $entity
        ));

    }

}
<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Entity\PersonWizyta;
use Ls\CmsBundle\Entity\KarnetZamowienie;
use Ls\CmsBundle\Form\PersonWizytaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\NotBlank;

class TrainingsController extends Controller {



    public function listAction(){

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $items = $qb
            ->select('n')
            ->from('LsCmsBundle:Trainings', 'n')
            ->where($qb->expr()->isNotNull('n.published_at'))
            ->orderBy('n.published_at', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Trainings:list.html.twig', array(
            'entities' => $items
        ));

    }

    public function detailAction($slug,Request $request){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('LsCmsBundle:Trainings')->findOneBySlug($slug);

        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('lscms_trainings_details',array('slug'=>$entity->getSlug())))
            ->setMethod('POST')
            ->add('content', 'textarea', array(
                    'label' => 'Treść Twojej wiadomości...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('city', null, array(
                    'label' => 'Miasto',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('dateFrom', null, array(
                    'label' => 'Data od',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('dateTo', null, array(
                    'label' => 'Data do',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('phone', null, array(
                    'label' => 'Phone',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('subject', 'hidden', array(
                    'data' => $entity->getTitle()
                )
            )
            ->add('name', null, array(
                    'label' => 'Twoje imię...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole'))
                    )
                )
            )
            ->add('email', null, array(
                    'label' => 'E-mail zwrotny...',
                    'required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Wypełnij pole')),
                        new Email(array('message' => 'Podaj poprawny adres e-mail'))
                    )
                )
            )
            ->getForm();

        $form->handleRequest($request);

        $success = false;
        if ($form->isValid()) {

            $email = $form->get('email')->getData();

            $message_txt = '<h3>Wiadomość z formularza szkoleń: '.$form->get('subject')->getData().'</h3>';
            $message_txt .= nl2br($form->get('content')->getData()) . ' <hr />';
            $message_txt .= '<h3>Pozostałe dane:</h3>';
            $message_txt .= 'Imię: ' . $form->get('name')->getData() . '<br />';
            $message_txt .= 'E-mail: ' . $form->get('email')->getData() . '<br />';
            $message_txt .= 'Telefon: ' . $form->get('phone')->getData() . '<br />';
            $message_txt .= 'Miasto: ' . $form->get('city')->getData() . '<br />';
            $message_txt .= 'Data od: ' . $form->get('dateFrom')->getData() . '<br />';
            $message_txt .= 'Data do: ' . $form->get('dateTo')->getData() . '<br />';

            $email_to = $em->getRepository('LsCmsBundle:Setting')->findOneByLabel('email_to_kontakt')->getValue();
            $email_to = 'lpiwowar@gmail.com';

            $message = \Swift_Message::newInstance()
                ->setSubject('Wiadomość z formularza kontaktowego:'.$form->get('subject')->getData())
                ->setFrom(array($this->container->getParameter('mailer_user') => 'Onyxx / Mailer'))
                ->setTo($email_to)
                ->setBody($message_txt, 'text/html')
                ->addPart(strip_tags($message_txt), 'text/plain');

            if (!empty($email)) {
                $message->setReplyTo($email);
            }

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);
            $success = true;
        }


        return $this->render('LsCmsBundle:Trainings:detail.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView()
        ));

    }

}
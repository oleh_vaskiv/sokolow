<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * GalleryPhoto
 * @ORM\Table(name="gallery_photo")
 * @ORM\Entity
 */
class GalleryPhoto {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="photos")
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     * @var \Ls\CmsBundle\Entity\Gallery
     */
    private $gallery;

    /**
     * @Assert\File(maxSize="2097152")
     */
    private $file;

    protected $listWidth = 171;
    protected $listHeight = 112;

    protected $detailWidth = 171;
    protected $detailHeight = 112;

    protected $verticalWidth = 171;
    protected $verticalHeight = 112;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return GalleryPhoto
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return GalleryPhoto
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer 
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set gallery
     *
     * @param \Ls\CmsBundle\Entity\Gallery $gallery
     * @return GalleryPhoto
     */
    public function setGallery(\Ls\CmsBundle\Entity\Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\CmsBundle\Entity\Gallery 
     */
    public function getGallery() {
        return $this->gallery;
    }

    public function __toString() {
        if (is_null($this->getPhoto())) {
            return 'NULL';
        }
        return $this->getPhoto();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
            case 'vertical':
                $size['width'] = $this->verticalWidth;
                $size['height'] = $this->verticalHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'vertical':
                    $sThumbName = Tools::thumbName($this->photo, '_v');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'vertical':
                    $sThumbName = Tools::thumbName($this->photo, '_v');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_d = Tools::thumbName($filename, '_d');
            $filename_v = Tools::thumbName($filename, '_v');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
            if (file_exists($filename_v)) {
                @unlink($filename_v);
            }
        }
    }

    public function getPhotoWebPath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return '/' . $this->getUploadDir() . '/' . $this->photo;
        }
    }

    public function getPhotoAbsolutePath() {
        if (null === $this->photo || empty($this->photo)) {
            return false;
        } else {
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
        }
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        if ($this->getGallery()) {
            return 'upload/gallery/' . $this->getGallery()->getId();
        } else {
            return 'upload/gallery';
        }
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        $options = array('jpegQuality' => 100);

        $thumb = ThumbFactory::create($sSourceName,$options);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName,$options);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        $thumb = ThumbFactory::create($sSourceName,$options);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);

        $thumb = ThumbFactory::create($sSourceName,$options);
        $sThumbNameV = Tools::thumbName($sSourceName, '_v');
        $aThumbSizeV = $this->getThumbSize('vertical');
        $thumb->adaptiveResize($aThumbSizeV['width'], $aThumbSizeV['height']);
        $thumb->save($sThumbNameV);

        unset($this->file);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $options = array('jpegQuality' => 100);
        $thumb = ThumbFactory::create($sSourceName,$options);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }


    public function rescale(){



    }

}
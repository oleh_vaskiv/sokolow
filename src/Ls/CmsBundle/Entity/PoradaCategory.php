<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * PoradaCategory
 * @ORM\Table(name="porada_category")
 * @ORM\Entity
 */
class PoradaCategory
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @Gedmo\Slug(fields={"color"})
     * @ORM\Column(type="string", length=7)
     * @var string
     */

    private $color;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Porada",
     *   mappedBy="category"
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $porady;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $menu_bg_img;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $thumb;

    protected $listWidth = 265;
    protected $listHeight = 200;
    protected $detailWidth = 408;
    protected $detailHeight = 200;

    protected $verticalWidth = 265;
    protected $verticalHeight = 421;

    protected $topWidth = 980;
    protected $topHeight = 276;

    protected $thumbWidth = 263;
    protected $thumbHeight = 71;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $description;


    /**
     * Constructor
     */
    public function __construct() {
        $this->porady = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PoradaCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return PoradaCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return PoradaCategory
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }


    /**
     * Set color
     *
     * @param string $color
     * @return PoradaCategory
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Get color in RGB
     *
     * @return string
     */
    public function getColorRGB()
    {
        $color = $this->color;

        list($r, $g, $b) = array($color[0].$color[1],
            $color[2].$color[3],
            $color[4].$color[5]);
        $r = hexdec($r); $g = hexdec($g); $b = hexdec($b);
        return array($r, $g, $b);
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Porada
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }


    /**
     * Set menu_bg_img
     *
     * @param string $menu_bg_img
     * @return Porada
     */
    public function setMenuBgImg($menu_bg_img) {
        $this->menu_bg_img = $menu_bg_img;

        return $this;
    }

    /**
     * Get menu_bg_img
     *
     * @return string
     */
    public function getMenuBgImg() {
        return $this->menu_bg_img;
    }


    /**
     * Set description
     *
     * @param string $description
     * @return Porada
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set thumb
     *
     * @param UploadedFile $thumb
     * @return Porada
     */

    public function setThumb(UploadedFile $thumb = null) {
        $this->deleteThumb();
        $this->thumb = $thumb;
        if (empty($this->thumb)) {
            $this->setMenuBgImg('empty');
        } else {
            $this->setMenuBgImg('');
        }
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb() {
        return $this->thumb;
    }



    /**
     * Add porady
     *
     * @param \Ls\CmsBundle\Entity\Porada $porada
     * @return PoradaCategory
     */
    public function addPorady(\Ls\CmsBundle\Entity\Porada $porada)
    {
        $this->porady[] = $porada;

        return $this;
    }

    /**
     * Remove porady
     *
     * @param \Ls\CmsBundle\Entity\Porada $porada
     */
    public function removePorady(\Ls\CmsBundle\Entity\Porada $porada)
    {
        $this->porady->removeElement($porada);
    }

    /**
     * Get porady
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPorady()
    {
        return $this->porady;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
            case 'vertical':
                $size['width'] = $this->verticalWidth;
                $size['height'] = $this->verticalHeight;
                break;
            case 'top':
                $size['width'] = $this->topWidth;
                $size['height'] = $this->topHeight;
                break;
            case 'thumb':
                $size['width'] = $this->thumbWidth;
                $size['height'] = $this->thumbHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'vertical':
                    $sThumbName = Tools::thumbName($this->photo, '_v');
                    break;
                case 'top':
                    $sThumbName = Tools::thumbName($this->photo, '_t');
                    break;

            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getMenuBgPath() {
        if (empty($this->menu_bg_img)) {
            return null;
        } else {

            $sThumbName = Tools::thumbName($this->menu_bg_img, '_th');

        }
        return '/' . $this->getUploadDir() . '/' . $sThumbName;

    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            switch ($type) {
                case 'list':
                    $sThumbName = Tools::thumbName($this->photo, '_l');
                    break;
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo, '_d');
                    break;
                case 'vertical':
                    $sThumbName = Tools::thumbName($this->photo, '_v');
                    break;
                case 'top':
                    $sThumbName = Tools::thumbName($this->photo, '_v');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_l = Tools::thumbName($filename, '_l');
            $filename_d = Tools::thumbName($filename, '_d');
            $filename_v = Tools::thumbName($filename, '_v');
            $filename_t = Tools::thumbName($filename, '_t');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_l)) {
                @unlink($filename_l);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
            if (file_exists($filename_v)) {
                @unlink($filename_v);
            }
            if (file_exists($filename_t)) {
                @unlink($filename_t);
            }
        }
    }

    public function deleteThumb() {
        // TODO: sth is messed up, after delete files still exists
        if (!empty($this->thumb)) {
            $filename = $this->getMenuBgAbsolutePath();

            $filename_th = Tools::thumbName($filename, '_th');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_th)) {
                @unlink($filename_th);
            }

        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getMenuBgAbsolutePath() {
        return empty($this->menu_bg_img) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->menu_bg_img;
    }



    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }



    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/category';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameL = Tools::thumbName($sSourceName, '_l');
        $aThumbSizeL = $this->getThumbSize('list');
        $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameV = Tools::thumbName($sSourceName, '_v');
        $aThumbSizeV = $this->getThumbSize('vertical');
        $thumb->adaptiveResize($aThumbSizeV['width'], $aThumbSizeV['height']);
        $thumb->save($sThumbNameV);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameT = Tools::thumbName($sSourceName, '_t');
        $aThumbSizeT = $this->getThumbSize('top');
        $thumb->adaptiveResize($aThumbSizeT['width'], $aThumbSizeT['height']);
        $thumb->save($sThumbNameT);

        unset($this->file);
    }

    public function uploadThumb() {
        if (null === $this->thumb) {
            return;
        }

        $sFileName = $this->getMenuBgImg();

        $this->thumb->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameL = Tools::thumbName($sSourceName, '_th');
        $aThumbSizeL = $this->getThumbSize('thumb');
        $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
        $thumb->save($sThumbNameL);



        unset($this->thumb);
    }


    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }


}
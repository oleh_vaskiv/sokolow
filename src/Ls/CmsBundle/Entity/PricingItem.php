<?php

namespace Ls\CmsBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PricingItem
 * @ORM\Table(name="pricing_item")
 * @ORM\Entity
 */
class PricingItem {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $promo_description;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     */
    private $promo_price;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $karnet;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\OneToMany(
     *   targetEntity="KarnetZamowienie",
     *   mappedBy="karnet",
     *   cascade={"all"}
     * )
     * @ORM\OrderBy({"created_at" = "DESC"})
     * @var \Doctrine\Common\Collections\Collection
     */
    private $zamowienia;

    /**
     * Constructor
     */
    public function __construct() {
        $this->karnet = false;
        $this->zamowienia = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PricingItem
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PricingItem
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set promo_description
     *
     * @param string $promo_description
     * @return PricingItem
     */
    public function setPromoDescription($promo_description) {
        $this->promo_description = $promo_description;

        return $this;
    }

    /**
     * Get promo_description
     *
     * @return string
     */
    public function getPromoDescription() {
        return $this->promo_description;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return PricingItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set promo_price
     *
     * @param string $promo_price
     * @return PricingItem
     */
    public function setPromoPrice($promo_price)
    {
        $this->promo_price = $promo_price;

        return $this;
    }

    /**
     * Get promo_price
     *
     * @return string
     */
    public function getPromoPrice()
    {
        return $this->promo_price;
    }

    /**
     * Set karnet
     *
     * @param boolean $karnet
     * @return PricingItem
     */
    public function setKarnet($karnet) {
        $this->karnet = $karnet;

        return $this;
    }

    /**
     * Get karnet
     *
     * @return boolean
     */
    public function getKarnet() {
        return $this->karnet;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return PricingItem
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Add zamowienia
     *
     * @param \Ls\CmsBundle\Entity\KarnetZamowienie $zamowienia
     * @return PricingItem
     */
    public function addZamowienia(\Ls\CmsBundle\Entity\KarnetZamowienie $zamowienia)
    {
        $this->zamowienia[] = $zamowienia;
    
        return $this;
    }

    /**
     * Remove zamowienia
     *
     * @param \Ls\CmsBundle\Entity\KarnetZamowienie $zamowienia
     */
    public function removeZamowienia(\Ls\CmsBundle\Entity\KarnetZamowienie $zamowienia)
    {
        $this->zamowienia->removeElement($zamowienia);
    }

    /**
     * Get zamowienia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getZamowienia()
    {
        return $this->zamowienia;
    }
    
    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }

}
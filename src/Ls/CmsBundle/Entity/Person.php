<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use PhpThumb\ThumbFactory;
use Ls\CmsBundle\Utils\Tools;

/**
 * Person
 * @ORM\Table(name="person")
 * @ORM\Entity
 */
class Person
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_main;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $dietetyk;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo_list;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo_main;

    /**
     * @ORM\OneToMany(
     *   targetEntity="PersonWizyta",
     *   mappedBy="person",
     *   cascade={"all"}
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $wizyty;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file_list;

    /**
     * @Assert\File(maxSize="2097152")
     */
    protected $file_main;

    protected $listWidth = 267;
    protected $listHeight = 245;
    protected $mainWidth = 146;
    protected $mainHeight = 218;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dietetyk = false;
        $this->wizyty = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Person
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Person
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set content_main
     *
     * @param string $content_main
     * @return Person
     */
    public function setContentMain($content_main)
    {
        $this->content_main = $content_main;

        return $this;
    }

    /**
     * Get content_main
     *
     * @return string
     */
    public function getContentMain()
    {
        return $this->content_main;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Person
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set photo_list
     *
     * @param string $photo_list
     * @return Person
     */
    public function setPhotoList($photo_list)
    {
        $this->photo_list = $photo_list;

        return $this;
    }

    /**
     * Get photo_list
     *
     * @return string
     */
    public function getPhotoList()
    {
        return $this->photo_list;
    }

    /**
     * Set photo_main
     *
     * @param string $photo_main
     * @return Person
     */
    public function setPhotoMain($photo_main)
    {
        $this->photo_main = $photo_main;

        return $this;
    }

    /**
     * Get photo_main
     *
     * @return string
     */
    public function getPhotoMain()
    {
        return $this->photo_main;
    }

    /**
     * Set dietetyk
     *
     * @param boolean $dietetyk
     * @return Person
     */
    public function setDietetyk($dietetyk)
    {
        $this->dietetyk = $dietetyk;

        return $this;
    }

    /**
     * Get dietetyk
     *
     * @return boolean
     */
    public function getDietetyk()
    {
        return $this->dietetyk;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Person
     */
    public function setArrangement($arrangement)
    {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement()
    {
        return $this->arrangement;
    }

    /**
     * Add wizyty
     *
     * @param \Ls\CmsBundle\Entity\PersonWizyta $wizyta
     * @return Person
     */
    public function addWizyty(\Ls\CmsBundle\Entity\PersonWizyta $wizyta)
    {
        $this->wizyty[] = $wizyta;

        return $this;
    }

    /**
     * Remove wizyty
     *
     * @param \Ls\CmsBundle\Entity\PersonWizyta $wizyta
     */
    public function removeWizyty(\Ls\CmsBundle\Entity\PersonWizyta $wizyta)
    {
        $this->wizyty->removeElement($wizyta);
    }

    /**
     * Get wizyty
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWizyty()
    {
        return $this->wizyty;
    }

    public function __toString()
    {
        if (is_null($this->getFirstname()) || is_null($this->getLastname())) {
            return 'NULL';
        }
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    public function setFileList(UploadedFile $file = null)
    {
        $this->deletePhoto('list');
        $this->file_list = $file;
        if (empty($this->photo_list)) {
            $this->setPhotoList('empty');
        } else {
            $this->setPhotoList('');
        }
    }

    public function getFileList()
    {
        return $this->file_list;
    }

    public function setFileMain(UploadedFile $file = null)
    {
        $this->deletePhoto('main');
        $this->file_main = $file;
        if (empty($this->photo_main)) {
            $this->setPhotoMain('empty');
        } else {
            $this->setPhotoMain('');
        }
    }

    public function getFileMain()
    {
        return $this->file_main;
    }

    public function getThumbSize($type)
    {
        $size = array();
        switch ($type) {
            case 'list':
                $size['width'] = $this->listWidth;
                $size['height'] = $this->listHeight;
                break;
            case 'main':
                $size['width'] = $this->mainWidth;
                $size['height'] = $this->mainHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type)
    {
        switch ($type) {
            case 'list':
                if (empty($this->photo_list)) {
                    return null;
                } else {
                    $sThumbName = Tools::thumbName($this->photo_list, '_t');
                    return '/' . $this->getUploadDir() . '/' . $sThumbName;
                }
                break;
            case 'main':
                if (empty($this->photo_main)) {
                    return null;
                } else {
                    $sThumbName = Tools::thumbName($this->photo_main, '_t');
                    return '/' . $this->getUploadDir() . '/' . $sThumbName;
                }
                break;
        }
    }

    public function getThumbAbsolutePath($type)
    {
        switch ($type) {
            case 'list':
                if (empty($this->photo_list)) {
                    return null;
                } else {
                    $sThumbName = Tools::thumbName($this->photo_list, '_t');
                    return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
                }
                break;
            case 'main':
                if (empty($this->photo_main)) {
                    return null;
                } else {
                    $sThumbName = Tools::thumbName($this->photo_main, '_t');
                    return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
                }
                break;
        }
    }

    public function getPhotoSize($type)
    {
        $temp = getimagesize($this->getPhotoAbsolutePath($type));
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function deletePhoto($type)
    {
        switch ($type) {
            case 'list':
                if (!empty($this->photo_list)) {
                    $filename = $this->getPhotoAbsolutePath($type);
                    $filename_t = Tools::thumbName($filename, '_t');
                    if (file_exists($filename)) {
                        @unlink($filename);
                    }
                    if (file_exists($filename_t)) {
                        @unlink($filename_t);
                    }
                }
                break;
            case 'main':
                if (!empty($this->photo_main)) {
                    $filename = $this->getPhotoAbsolutePath($type);
                    $filename_t = Tools::thumbName($filename, '_t');
                    if (file_exists($filename)) {
                        @unlink($filename);
                    }
                    if (file_exists($filename_t)) {
                        @unlink($filename_t);
                    }
                }
                break;
        }
    }

    public function getPhotoAbsolutePath($type)
    {
        switch ($type) {
            case 'list':
                return empty($this->photo_list) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo_list;
                break;
            case 'main':
                return empty($this->photo_main) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo_main;
                break;
        }
    }

    public function getPhotoWebPath($type)
    {
        switch ($type) {
            case 'list':
                return empty($this->photo_list) ? null : '/' . $this->getUploadDir() . '/' . $this->photo_list;
                break;
            case 'main':
                return empty($this->photo_main) ? null : '/' . $this->getUploadDir() . '/' . $this->photo_main;
                break;
        }
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/team';
    }

    public function upload($type)
    {
        switch ($type) {
            case 'list':
                if (null === $this->file_list) {
                    return;
                }

                $sFileName = $this->getPhotoList();
                $this->file_list->move($this->getUploadRootDir(), $sFileName);
                break;

            case 'main':
                if (null === $this->file_main) {
                    return;
                }

                $sFileName = $this->getPhotoMain();
                $this->file_main->move($this->getUploadRootDir(), $sFileName);
                break;

            default:
                return;
                break;
        }


        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbName = Tools::thumbName($sSourceName, '_t');
        $aThumbSize = $this->getThumbSize($type);
        $thumb->adaptiveResize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);

        switch ($type) {
            case 'list':
                unset($this->file_list);
                break;

            case 'main':
                unset($this->file_main);
                break;
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type)
    {
        switch ($type) {
            case 'list':
                $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhotoList();
                break;

            case 'main':
                $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhotoMain();
                break;

            default:
                return;
                break;
        }
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Lukasz
 * Date: 08.10.14
 * Time: 18:19
 */
namespace Ls\CmsBundle\Entity;

interface PhotoEntityInterface{

    public function setPhoto($photo);
    public function getPhoto();

}
<?php
/**
 * Created by PhpStorm.
 * User: Lukasz
 * Date: 08.10.14
 * Time: 16:49
 */

namespace Ls\CmsBundle\Entity;
use PhpThumb\ThumbFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ls\CmsBundle\Utils\Tools;

abstract class BaseEntity implements PhotoEntityInterface {

    public $imagesSizes = array();

    public function getThumbSize($type) {

        return $this->imagesSizes[$type];

    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = Tools::thumbName($this->photo, $type);

            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = Tools::thumbName($this->photo, $type);

            $directPath = $this->getThumbAbsolutePath($type);

            if(!file_exists($directPath)){
                $filePath = $this->getPhotoAbsolutePath();
                $thumb = ThumbFactory::create($filePath);
                $sThumbNameA = Tools::thumbName($filePath, $type);
                $aThumbSizeA = $this->getThumbSize($type);
                $thumb->adaptiveResize($aThumbSizeA['width'], $aThumbSizeA['height']);
                $thumb->save($sThumbNameA);
            }

            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {

        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        //return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
        //var_dump(__DIR__);
        //var_dump($_SERVER['DOCUMENT_ROOT']);
       // die();
        //return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
        return $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->getUploadDir();

    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.

        return strtolower('upload/'.end(explode('\\',get_class($this))));

    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            foreach($this->imagesSizes as $key => $obj){
                $filename_to_unlink = Tools::thumbName($filename, $key);

                if (file_exists($filename_to_unlink)) {
                    @unlink($filename_to_unlink);
                }
            }
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function Thumb($x, $y, $x2, $y2, $type) {

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);

        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);

    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1024, 768);
            $thumb->save($sSourceName);
        }

        foreach($this->imagesSizes as $key => $obj){

            //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
            $thumb = ThumbFactory::create($sSourceName);
            $sThumbNameL = Tools::thumbName($sSourceName, $key);
            $aThumbSizeL = $this->getThumbSize($key);
            $thumb->adaptiveResize($aThumbSizeL['width'], $aThumbSizeL['height']);
            $thumb->save($sThumbNameL);

        }

        unset($this->file);
    }

} 
<?php

namespace Ls\CmsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * KarnetZamowienie
 * @ORM\Table(name="karnet_zamowienie")
 * @ORM\Entity
 */
class KarnetZamowienie
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $document;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $ankieta;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="PricingItem",
     *     inversedBy="zamowienia"
     * )
     * @ORM\JoinColumn(
     *     name="karnet_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var \Ls\CmsBundle\Entity\PricingItem
     */
    private $karnet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return KarnetZamowienie
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return KarnetZamowienie
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return KarnetZamowienie
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return KarnetZamowienie
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return KarnetZamowienie
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set ankieta
     *
     * @param string $ankieta
     * @return KarnetZamowienie
     */
    public function setAnkieta($ankieta)
    {
        $this->ankieta = $ankieta;

        return $this;
    }

    /**
     * Get ankieta
     *
     * @return string
     */
    public function getAnkieta()
    {
        return $this->ankieta;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return KarnetZamowienie
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return KarnetZamowienie
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set karnet
     *
     * @param \Ls\CmsBundle\Entity\PricingItem $karnet
     * @return KarnetZamowienie
     */
    public function setKarnet(\Ls\CmsBundle\Entity\PricingItem $karnet = null)
    {
        $this->karnet = $karnet;

        return $this;
    }

    /**
     * Get karnet
     *
     * @return \Ls\CmsBundle\Entity\PricingItem
     */
    public function getKarnet()
    {
        return $this->karnet;
    }

    public function __toString()
    {
        if (is_null($this->getCode())) {
            return 'NULL';
        }
        return $this->getCode();
    }

}
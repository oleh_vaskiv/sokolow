<?php

// src/LS/CMSBundle/Admin/SliderAdmin.php

namespace LS\CMSBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SliderAdmin extends Admin {

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'created_at'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('photos', $this->getRouterIdParameter() . '/photos');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Ogólne')
                ->add('label', null, array('label' => 'Etykieta'))
                ->add('photo_width', null, array('label' => 'Szerokość zdjęcia'))
                ->add('photo_height', null, array('label' => 'Wysokość zdjęcia'))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('label', null, array('label' => 'Etykieta'))
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('label', null, array('label' => 'Etykieta'))
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'photos' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Slider:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}

<?php

// src/Ls/CmsBundle/Admin/MenuItemAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class MenuItemAdmin extends Admin {

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('routeparameters');
        $collection->add('saveorder');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Element menu')
                ->add('title', null, array('label' => 'Tytuł', 'required' => true))
                ->add('type', 'choice', array('choices' => $this->getTypeChoices(), 'label' => 'Typ linku'))
                ->add('location', 'hidden', array())
                ->add('url', null, array('label' => 'URL'))
                ->add('route', 'choice', array('required' => false, 'choices' => $this->getModuleChoices(), 'label' => 'Moduł'))
                ->add('routeParameters', 'hidden', array('required' => false))
                ->add('onclick', 'choice', array('required' => false, 'choices' => $this->getOnClickChoices(), 'label' => 'Otwierany obiekt'))
                ->add('searchServices', 'hidden', array('data' => $this->getSearchServicesJson(), 'mapped' => false), array('style' => ''))
                ->end()
        ;
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('_action', 'actions', array(
                    'label' => 'Opcje',
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                ))
        ;
    }

    public function getTypeChoices() {
        $types = array(
            'url' => 'Adres URL',
            'route' => 'Moduł',
            'button' => 'Przycisk JavaScript'
        );

        return $types;
    }

    public function getLocationChoices() {

        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $locations = array();

        foreach ($config_menu['locations'] as $location) {

            $locations[$location['label']] = $location['name'];
        }

        return $locations;
    }

    public function getModuleChoices() {

        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $modules = array();

        foreach ($config_menu['modules'] as $module) {

            $modules[$module['route']] = $module['label'];
        }

        return $modules;
    }

    public function getOnClickChoices() {
        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $functions = array();

        foreach ($config_menu['onclick'] as $function) {
            $functions[$function['name']] = $function['label'];
        }

        return $functions;
    }

    public function getSearchServicesJson() {
        return json_encode($this->getSearchServices());
    }

    public function getSearchServices() {
        $config_menu = $this->getConfigurationPool()->getContainer()->getParameter('ls_cms.menu');

        $services = array();
        foreach ($config_menu['modules'] as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\MenuItem:edit.html.twig';
                break;

            case 'delete':
                return 'LsCmsBundle:Admin\MenuItem:delete.html.twig';
                break;

            case 'list':
                return 'LsCmsBundle:Admin\MenuItem:list.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

}

<?php

// src/Ls/CmsBundle/Admin/PoradaAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProjectAdmin extends Admin
{

    // setup the default sort column and order
    protected $datagridValues = array(
        '_sort_order' => 'DESC',
       // '_sort_by' => 'published_at'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Gallery');
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('LsCmsBundle:Gallery', 'g')
            ->where('g.attachable = 1')
            ->getQuery();

        $helps = array();
        $formMapper
            ->with('Treść')
            ->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący projekt np. http://www.test.pl/projekty/<b>projekt</b>';
        }
        $formMapper
            ->add('published_at', 'date', array('label' => 'Data publikacji', 'required' => false))
            ->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg-basic')))
            ->add('gallery', 'sonata_type_model', array('query' => $query, 'label' => 'Galeria', 'label' => 'Galeria', 'required' => false))
            ->add('seo_generate', null, array('label' => 'Generuj opcje SEO'))
            ->add('seo_title', null, array('label' => 'SEO Title', 'required' => false))
            ->add('seo_keywords', 'textarea', array('label' => 'SEO Keywords', 'required' => false))
            ->add('seo_description', 'textarea', array('label' => 'SEO Description', 'required' => false, 'attr' => array('rows' => 3)))
            ->with('Zdjęcie');
        if (null !== $this->getRoot()->getSubject()->getPhoto()) {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        } else {
            $formMapper
                ->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => true));
        }
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Tytuł'))
            ->add('content', null, array('label' => 'Treść'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('title', null, array('label' => 'Tytuł'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Project:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('porada-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('porada-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->upload();
        }
    }
}

<?php

// src/Ls/CmsBundle/Admin/DoradztwoSettingsAdmin.php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TextsSettingAdmin extends Admin
{

    protected $baseRouteName = 'admin_ls_cms_texts_settings';
    protected $baseRoutePattern = 'texts-settings';

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        $qb = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Setting')->createQueryBuilder();

        $query->where($qb->expr()->like('o.label', $qb->expr()->literal('added_texts_%')));
        $query->orderBy('o.label', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne')
            ->add('label', 'hidden', array('label' => 'Etykieta'))
            ->add('description', null, array('label' => 'Opis', 'required' => false))
            ->add('value', null, array('label' => 'Wartość', 'required' => false, 'attr' => array('class' => 'wysiwyg-basic')));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('description', null, array('label' => 'Opis'))
            ->add('value', null, array('label' => 'Wartość'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description', null, array('label' => 'Opis'))
            ->add('value', null, array('label' => 'Wartość', 'template' => 'LsCmsBundle:Admin\Texts:list_content.html.twig'))
            ->add('_action', 'actions', array(
                'label' => 'Opcje',
                'actions' => array(
                    'edit' => array(),
                )
            ));
    }

}

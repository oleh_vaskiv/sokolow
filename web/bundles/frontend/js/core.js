/**
 * Created by Lukasz on 26.10.14.
 */



$(function(){

    $('.home').hover(function() {
        $(this).find('span').stop(true, false).animate({'opacity': 1}, 500);
    }, function() {
        $(this).find('span').stop(true, false).animate({'opacity': 0}, 500);
    });

    $("a.fancybox").fancybox({
        'transitionIn'	:	'elastic',
        'transitionOut'	:	'elastic',
        'speedIn'		:	600,
        'speedOut'		:	200,
        'onCleanup': function(){}
    });

    $('#footer td.home').remove();


    $('.gallery-details > .item').hover(function() {
        $(this).find('span').stop(true, false).animate({'opacity': 1}, 500);
    }, function() {
        $(this).find('span').stop(true, false).animate({'opacity': 0}, 500);
    });

});

